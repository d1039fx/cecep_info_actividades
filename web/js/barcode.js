function detectBarcode(dataUrl, callback) {

    // call here your favorite javascript barcode scan library
    // input must be an image dataUrl
    // output must be a single String

    // don't forget to trigger the call back in order to get the result

    // This method will trigger user permissions
    Html5Qrcode.getCameras().then(devices => {
      /**
       * devices would be an array of objects of type:
       * { id: "id", label: "label" }
       */
      if (devices && devices.length) {
        var cameraId = devices[0].id;
        // .. use this to start scanning.
      }
    }).catch(err => {
      // handle err
    });

    callback(barcode);
}