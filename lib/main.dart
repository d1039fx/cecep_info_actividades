import 'dart:io';

import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/presentation/bloc/estado_materias/estado_materias_bloc.dart';
import 'package:claseinfoactividades/grades/presenters/grades_data/grades_data_bloc.dart';
import 'package:claseinfoactividades/grades/presenters/grades_presenter/grades_presenter_bloc.dart';
import 'package:claseinfoactividades/login/login.dart';
import 'package:claseinfoactividades/provider/info_salon.dart';
import 'package:claseinfoactividades/salones/buscar_salon.dart';
import 'package:claseinfoactividades/salones/info_salones.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = ClasesHttpOverrides();

  if (kIsWeb) {
    print('es web');
    Hive.initFlutter();
    await Hive.openBox<String>('datos');
    await Hive.openBox<List<Map<String, dynamic>>>('escenario');
    runApp(CecepInfoActividades());
  } else {
    print('no es web');
    Directory document = await getApplicationDocumentsDirectory();
    Hive.init(document.path);
    await Hive.openBox<String>('datos');
    await Hive.openBox<List<Map<String, dynamic>>>('escenario');
    runApp(CecepInfoActividades());
  }
}

class CecepInfoActividades extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<InfoSalonesBusqueda>(
            create: (_) => InfoSalonesBusqueda())
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<GradesPresenterBloc>(
            create: (context) => GradesPresenterBloc(),
          ),
          BlocProvider<GradesDataBloc>(
            create: (context) => GradesDataBloc(),
          ),
          BlocProvider<EstadoMateriasBloc>(
            create: (context) => EstadoMateriasBloc(),
          ),
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.red,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: LoginUsuario(),
          routes: {
            'salon': (context) => InfoSalones(),
            'buscar_salon': (context) => BuscarSalon()
          },
        ),
      ),
    );
  }
}

class ClasesHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
