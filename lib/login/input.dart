import 'package:flutter/material.dart';

class EntradaDatos extends StatelessWidget {
  final TextEditingController formField;
  final String nombreCampo;

  const EntradaDatos({required Key key, required this.formField, required this.nombreCampo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: formField,
      obscureText: nombreCampo != 'Documento de identificación' ? false : true,
      keyboardType: TextInputType.number,
      decoration:
          InputDecoration(hintText: nombreCampo, border: OutlineInputBorder(borderRadius: BorderRadius.circular(20))),
      validator: (valor) {
        if (valor == null) {
          return 'Campo vacio';
        }
        return null;
      },
    );
  }
}
