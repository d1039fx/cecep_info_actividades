import 'dart:convert';

import 'package:claseinfoactividades/clases/mostrar_datos_login_model.dart';
import 'package:claseinfoactividades/login/input.dart';
import 'package:claseinfoactividades/pagina_principal/inicial.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

class LoginUsuario extends StatefulWidget {
  static TextEditingController _codigoEstudiante = TextEditingController();
  static TextEditingController _cedula = TextEditingController();
  static GlobalKey<FormState> _form = GlobalKey<FormState>();

  @override
  _LoginUsuarioState createState() => _LoginUsuarioState();
}

class _LoginUsuarioState extends State<LoginUsuario> {
  late Box<String> _datos;
  late Box<List<Map<String, dynamic>>> _listaEscenarios;
  late String respuestaLogin;
  late Future<List<Map<String, dynamic>>> escenarios;

  List<InformacionUsuario> parseInfo(String respuesta) {
    final List<Map<String, dynamic>> parsed =
        jsonDecode(respuesta).cast<Map<String, dynamic>>();
    return parsed
        .map<InformacionUsuario>((valor) => InformacionUsuario.fromJson(valor))
        .toList();
  }

  Future<http.Response> pruebaHttp({required String codigo, required String documento}) async {
    Uri url =
        Uri.https('servicios.cecep.edu.co', '/consultas/horarios_json.asp');
    http.Response respuesta = await http.post(url, body: {'codigo': codigo});
    return respuesta;
  }

  Future<List<Map<String, dynamic>>> infoEscenariosHttp(
      {required String urlEscenarios}) async {
    Uri url = Uri.https('servicios.cecep.edu.co', urlEscenarios);
    http.Response respuesta = await http.get(url);
    List<Map<String, dynamic>> lista =
        await jsonDecode(respuesta.body).cast<Map<String, dynamic>>();
    return lista;
  }

  Future<List<Map<String, dynamic>>> infoTodoHttp(
      {required String salones,
      required String salas,
      required String auditorios,
      required String laboratorios}) async {
    List<Map<String, dynamic>> listaSalones =
        await infoEscenariosHttp(urlEscenarios: salones);
    List<Map<String, dynamic>> listaSalas =
        await infoEscenariosHttp(urlEscenarios: salas);
    List<Map<String, dynamic>> listaAuditorios =
        await infoEscenariosHttp(urlEscenarios: auditorios);
    List<Map<String, dynamic>> listaLaboratorios =
        await infoEscenariosHttp(urlEscenarios: laboratorios);
    return listaSalones + listaSalas + listaAuditorios + listaLaboratorios;
  }

  @override
  void initState() {
    super.initState();
    respuestaLogin = '';
    _datos = Hive.box<String>('datos');
    _listaEscenarios = Hive.box<List<Map<String, dynamic>>>('escenario');
    escenarios = infoTodoHttp(
        salones: '/consultas/salones_cecep_json.asp',
        auditorios: '/consultas/auditorios_cecep_json.asp',
        laboratorios: '/consultas/laboratorios_cecep_json.asp',
        salas: '/consultas/salas_cecep_json.asp');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.red, Colors.red[600]!, Colors.red[900]!],
                stops: [0.1, 0.3, 0.7],
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter)),
        child: SingleChildScrollView(
          child: Form(
            key: LoginUsuario._form,
            child: Column(
              children: [
                Card(
                  margin: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Image.asset(
                          'assets/imagenes/cecep.png',
                          height: 200,
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        margin: const EdgeInsets.all(10),
                        child: EntradaDatos(
                          formField: LoginUsuario._codigoEstudiante,
                          nombreCampo: 'Codigo Estudiante', key: Key('value'),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        margin: const EdgeInsets.all(10),
                        child: EntradaDatos(
                          formField: LoginUsuario._cedula,
                          nombreCampo: 'Documento de identificación', key: Key('value'),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(10),
                        child: ElevatedButton(
                            onPressed: () {
                              if (LoginUsuario._form.currentState!.validate()) {
                                pruebaHttp(
                                        codigo:
                                            LoginUsuario._codigoEstudiante.text,
                                        documento: LoginUsuario._cedula.text)
                                    .then((value) {
                                  if (value.body.isEmpty) {
                                  } else {
                                    print(parseInfo(value.body)[0].documento);
                                    setState(() {
                                      respuestaLogin =
                                          'No hay datos del usuario';
                                    });
                                  }
                                  switch (value.statusCode) {
                                    case 302:
                                      print(
                                          'La página que intentas ingresar no se encuentra disponible actualmente.');
                                      setState(() {
                                        respuestaLogin =
                                            'El codigo que intentas ingresar no se encuentra disponible actualmente.';
                                      });
                                      break;
                                    case 404:
                                      print(
                                          'La página que intentas ingresar no se encuentra disponible actualmente.');
                                      break;
                                    default:
                                      if (LoginUsuario._cedula.text ==
                                          parseInfo(value.body)[0].documento) {
                                        _datos.put('valor', value.body);
                                        escenarios.then((value) {
                                          _listaEscenarios.put(
                                              'lista_escenarios', value);
                                        });
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    PaginaInicial()));
                                      } else {
                                        setState(() {
                                          respuestaLogin =
                                              'El documento de identificación no corresponde a este usuario';
                                        });
                                      }
                                  }
                                }).catchError((error) {
                                  print(error);
                                  switch (error.toString()) {
                                    case 'RangeError (index): Invalid value: Valid value range is empty: 0':
                                      setState(() {
                                        respuestaLogin =
                                            'No hay datos del usuario';
                                      });
                                      break;
                                    case 'RangeError (index): Index out of range: no indices are valid: 0':
                                      setState(() {
                                        respuestaLogin =
                                            'No hay datos del usuario';
                                      });
                                      break;
                                    case 'XMLHttpRequest error.':
                                      setState(() {
                                        respuestaLogin =
                                        'No hay conexion a internet';
                                      });
                                      break;
                                    default:
                                      setState(() {

                                        respuestaLogin = 'El usuario no existe o no es valido';
                                        print(error.toString());
                                      });
                                  }
                                });
                              }
                            },
                            child: Text(
                              'Login',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    respuestaLogin,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
