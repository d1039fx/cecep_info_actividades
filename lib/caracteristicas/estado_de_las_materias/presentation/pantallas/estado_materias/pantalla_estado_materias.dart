import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/presentation/bloc/estado_materias/estado_materias_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PantallaEstadoMaterias extends StatelessWidget {
  const PantallaEstadoMaterias({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[600],
      appBar: AppBar(
        backgroundColor: Colors.red[600],
        iconTheme: IconThemeData(color: Colors.white),
        title: Text('Estado de las materias', style: TextStyle(color: Colors.white),),
      ),
      body: SingleChildScrollView(
        child: BlocBuilder<EstadoMateriasBloc, EstadoMateriasState>(
          builder: (context, state) {
            if (state is ListaEstadoMaterias) {
              List<List<String>> listaEstadoMaterias =
                  state.listaEstadoMaterias;
              return Column(
                children: [
                  Card(
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: Text(
                              'Materias Pendientes',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )),
                        Divider(
                          height: 0,
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Column(
                            children: listaEstadoMaterias[0]
                                .map((materias) => Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(materias,),
                                ))
                                .toList(),
                          ),
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: Text(
                              'Materias Vistas',
                              style: TextStyle(fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )),
                        Divider(
                          height: 0,
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Column(
                            children: listaEstadoMaterias[1]
                                .map((materias) => Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(materias),
                                ))
                                .toList(),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              );
            }
            if (state is ErrorEstadoMaterias) {
              return Center(
                child: Text(state.error),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
