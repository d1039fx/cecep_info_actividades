import 'package:bloc/bloc.dart';
import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/data/fuente_de_datos/fuente_datos_estado_materias_api.dart';
import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/data/repositorio/repositorio_estado_materias_impl.dart';
import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/dominio/entidades/estado_materias.dart';
import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/dominio/repositorios/estado_materias_repositorio.dart';
import 'package:claseinfoactividades/clases/fallas/falla_data.dart';
import 'package:claseinfoactividades/clases/resultados/resultado.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
part 'estado_materias_event.dart';
part 'estado_materias_state.dart';

class EstadoMateriasBloc
    extends Bloc<EstadoMateriasEvent, EstadoMateriasState> {
  List<String> listaDeMateriasVistas = [];
  List<String> listaDeMateriasPendientes = [];

  EstadoMateriasBloc() : super(EstadoMateriasInitial()) {
    on<GetUserData>((event, emit) async {
      final EstadoMateriasRepositorio estadoMateriasRepositorio =
          RepositorioEstadoMateriaImpl(
              remotofuenteDatosEstadoMaterias: FuenteDatosEstadoMateriasApi());

      emit(LoaderEstadoMaterias());
      final resultado = await estadoMateriasRepositorio.obtenerEstadoMaterias(client: event.client, codigoEstudiante: event.codigoEstudiante);
      if (resultado is Exito) {
        List<String> listaDemateriasPendientes = (((resultado as Exito).value) as EstadoMaterias).materias_pendientes.map((e) => e.toString()).toList();
        List<String> listaDeMateriasVistas = (((resultado as Exito).value) as EstadoMaterias).materias_vistas.map((e) => e.toString()).toList();

        List<List<String>> estadoMaterias = [listaDemateriasPendientes, listaDeMateriasVistas];
        emit(ListaEstadoMaterias(listaEstadoMaterias: estadoMaterias));
      }

      if (resultado is Error) {
        emit(ErrorEstadoMaterias(error: (((resultado as Error).value) as FallaData).message));
      }
    });
  }
}
