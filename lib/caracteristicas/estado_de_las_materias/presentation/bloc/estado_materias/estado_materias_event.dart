part of 'estado_materias_bloc.dart';



@immutable
sealed class EstadoMateriasEvent {}

class GetUserData extends EstadoMateriasEvent{
  final http.Client client;
  final String codigoEstudiante;

  GetUserData({required this.client, required this.codigoEstudiante});
}