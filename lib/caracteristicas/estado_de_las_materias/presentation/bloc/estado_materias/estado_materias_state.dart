part of 'estado_materias_bloc.dart';

@immutable
sealed class EstadoMateriasState {}

final class EstadoMateriasInitial extends EstadoMateriasState {}

final class ErrorEstadoMaterias extends EstadoMateriasState {
  final String error;

  ErrorEstadoMaterias({required this.error});
}

final class LoaderEstadoMaterias extends EstadoMateriasState {}

final class ListaEstadoMaterias extends EstadoMateriasState{
  final List<List<String>> listaEstadoMaterias;

  ListaEstadoMaterias({required this.listaEstadoMaterias});
}
