import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/dominio/entidades/estado_materias.dart';

class ModeloEstadoMaterias extends EstadoMaterias {
  ModeloEstadoMaterias(
      {required super.materias_pendientes, required super.materias_vistas});

  factory ModeloEstadoMaterias.fromJson(Map<String, dynamic> json) =>
      ModeloEstadoMaterias(
        materias_vistas: json['materias_vistas'],
        materias_pendientes: json['materias_pendientes'],
      );

  Map<String, dynamic> toJson() => {
        'materias_vistas': materias_vistas,
        'materias_pendientes': materias_pendientes
      };
}
