import 'dart:convert';

import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/data/fuente_de_datos/fuente_datos_estado_materias.dart';
import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/data/modelos/modelo_estado_materias.dart';
import 'package:http/http.dart';

class FuenteDatosEstadoMateriasApi extends FuenteDatosEstadoMaterias {
  @override
  Future<ModeloEstadoMaterias> obtenerEstadoMaterias(
      {required Client client, required String codigoEstudiante}) async {
    try {
      Response response = await client.post(Uri.parse(
          'https://servicios.cecep.edu.co/consultas/materias/consulta.php'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'codigo': codigoEstudiante,
        }),);

      if (response.statusCode == 200) {
        //print(response.body.replaceAll('<html><body><p>', '').replaceAll('&quot;', '"'));
        //print(jsonDecode(response.body.replaceAll('<html><body><p>', '').replaceAll('&quot;', '"').replaceAll('</p></body></html>', '')) as Map<String, String>);
        return ModeloEstadoMaterias.fromJson(
            jsonDecode(response.body.replaceAll('<html><body><p>', '').replaceAll('&quot;', '"').replaceAll('</p></body></html>', '')) as Map<String, dynamic>
            // {"materias_pendientes": <String>[], "materias_vistas": <String>[]}
        );
      } else {
        throw Exception(response.statusCode);
      }
    } catch (error) {
      throw Exception(error);
    }
  }
}
