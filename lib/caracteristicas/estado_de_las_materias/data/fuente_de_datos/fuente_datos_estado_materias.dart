import 'package:http/http.dart' as http;

import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/data/modelos/modelo_estado_materias.dart';

abstract class FuenteDatosEstadoMaterias {
  Future<ModeloEstadoMaterias> obtenerEstadoMaterias(
      {required http.Client client, required String codigoEstudiante});
}
