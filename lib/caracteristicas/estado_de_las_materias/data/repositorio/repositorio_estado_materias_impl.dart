import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/data/fuente_de_datos/fuente_datos_estado_materias.dart';

import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/dominio/entidades/estado_materias.dart';
import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/dominio/repositorios/estado_materias_repositorio.dart';
import 'package:claseinfoactividades/clases/fallas/falla_data.dart';

import 'package:claseinfoactividades/clases/resultados/resultado.dart';
import 'package:http/src/client.dart';

class RepositorioEstadoMateriaImpl implements EstadoMateriasRepositorio {
  final FuenteDatosEstadoMaterias _remotofuenteDatosEstadoMaterias;

  RepositorioEstadoMateriaImpl(
      {required FuenteDatosEstadoMaterias remotofuenteDatosEstadoMaterias})
      : _remotofuenteDatosEstadoMaterias = remotofuenteDatosEstadoMaterias;

  @override
  Future<Resultado<EstadoMaterias, FallaData>> obtenerEstadoMaterias({required Client client, required String codigoEstudiante}) async{
    try{
      final estadoMateria = await _remotofuenteDatosEstadoMaterias.obtenerEstadoMaterias(client: client, codigoEstudiante: codigoEstudiante);
      return Exito(value: estadoMateria);
    }catch (error){
      return Error(value: FallaData(message: error.toString()));
    }
  }
}
