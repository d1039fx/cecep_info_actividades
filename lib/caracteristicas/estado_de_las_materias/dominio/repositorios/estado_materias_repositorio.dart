import 'package:claseinfoactividades/caracteristicas/estado_de_las_materias/dominio/entidades/estado_materias.dart';
import 'package:claseinfoactividades/clases/fallas/falla_data.dart';
import 'package:claseinfoactividades/clases/resultados/resultado.dart';
import 'package:http/http.dart' as http;

abstract class EstadoMateriasRepositorio {
  Future<Resultado<EstadoMaterias, FallaData>> obtenerEstadoMaterias(
      {required http.Client client, required String codigoEstudiante});
}
