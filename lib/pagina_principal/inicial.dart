import 'dart:io';

import 'package:claseinfoactividades/clases/constantes.dart';
import 'package:claseinfoactividades/clases/mostrar_datos.dart';
import 'package:claseinfoactividades/clases/mostrar_datos_login_model.dart';
import 'package:claseinfoactividades/salones/horarios_salon.dart';
import 'package:claseinfoactividades/salones/mapa_salones_ubicacion.dart';
import 'package:claseinfoactividades/widgets/drawer_widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class PaginaInicial extends StatefulWidget {
  static Box<String> _datos = Hive.box('datos');

  static MostrarDatos _mostrarDatos = MostrarDatos();

  @override
  _PaginaInicialState createState() => _PaginaInicialState();
}

class _PaginaInicialState extends State<PaginaInicial> with MostrarDatos {
  bool mostrarMapa = false;

  late DrawerWidgets drawerWidgets;
  Constantes constantes = Constantes();

  InformacionUsuario datosUsuario() {
    String? listaDatos = PaginaInicial._datos.get('valor');
    InformacionUsuario datos =
        PaginaInicial._mostrarDatos.parseInfo(listaDatos!)[0];
    return datos;
  }

  List<InformacionUsuario> listaMaterias() {
    String? listaDatos = PaginaInicial._datos.get('valor');
    List<InformacionUsuario> datos =
        PaginaInicial._mostrarDatos.parseInfo(listaDatos!);
    return datos;
  }

  @override
  void initState() {
    // TODO: implement initState
    drawerWidgets = DrawerWidgets(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[600],
      appBar: AppBar(
        title: Text('Horario'),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pushNamed('salon'),
            child: Column(
              children: [
                Icon(
                  Icons.select_all,
                  color: Colors.white,
                ),
                Text(
                  'Ver salon',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pushNamed('buscar_salon'),
            child: Column(
              children: [
                Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                Text(
                  'Buscar salon',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            DrawerHeader(
                child: Container(
              //color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Image.asset(
                          'assets/imagenes/cecep.png',
                          height: 60,
                        )),
                  ),
                  drawerWidgets.textDrawer(
                      fontSize: 13,
                      textData: datosUsuario().nombre,
                      fontWeight: FontWeight.bold),
                  drawerWidgets.textDrawer(
                      textData: datosUsuario().codigo,
                      fontSize: 12,
                      fontWeight: FontWeight.normal),
                  drawerWidgets.textDrawer(
                      textData: 'Semestre ${datosUsuario().semestre}',
                      fontSize: 12,
                      fontWeight: FontWeight.normal),
                  drawerWidgets.textDrawer(
                      textData: datosUsuario().plan,
                      fontSize: 12,
                      fontWeight: FontWeight.normal),
                ],
              ),
            )),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                alignment: Alignment.topCenter,
                //height: 180,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Espacios',
                        style: TextStyle(
                            color: Colors.grey[900],
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: TextButton(
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Auditorio Piso 3 Decanatura',
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Icon(
                              Icons.next_plan,
                              color: Colors.grey[600],
                            )
                          ],
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HorariosSalon(key: Key('value'),
                                    idSalon: '4408',
                                    tituloEspacio:
                                        'Auditorio Piso 3 Decanatura',
                                  )));
                        },
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.symmetric(vertical: 20)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Auditorio Piso 3',
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Icon(
                              Icons.next_plan,
                              color: Colors.grey[600],
                            )
                          ],
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HorariosSalon(
                                    idSalon: '7007',
                                    tituloEspacio: 'Auditorio Piso 3',
                                  )));
                        },
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.symmetric(vertical: 20)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Auditorio Piso 2',
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Icon(
                              Icons.next_plan,
                              color: Colors.grey[600],
                            )
                          ],
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HorariosSalon(
                                    idSalon: '4409',
                                    tituloEspacio: 'Auditorio Piso 2',
                                  )));
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Oficinas',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[900]),
                      ),
                    ),
                    Expanded(
                        child: drawerWidgets.officeList(
                            seccion: constantes.oficinas))
                    // Align(
                    //   child: TextButton(
                    //     onPressed: () {},
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Text(
                    //           'Coordinacion Academica',
                    //           style: TextStyle(color: Colors.grey[600]),
                    //         ),
                    //         Icon(
                    //           Icons.next_plan,
                    //           color: Colors.grey[600],
                    //         )
                    //       ],
                    //     ),
                    //     style: TextButton.styleFrom(
                    //         alignment: Alignment.centerLeft,
                    //         padding: const EdgeInsets.symmetric(vertical: 20)
                    //     ),
                    //   ),
                    // ),
                    // drawerWidgets.oficina(
                    //   function: null,
                    //   nameOficina: 'Coordinacion Academica'
                    // ),
                    // drawerWidgets.oficina(
                    //   function: null,
                    //   nameOficina: 'Director General Académico'
                    // ),
                    // drawerWidgets.oficina(
                    //     function: null,
                    //     nameOficina: 'Secretaria General'
                    // ),
                    // drawerWidgets.oficina(
                    //     function: null,
                    //     nameOficina: 'Secretaria General'
                    // ),
                  ],
                ),
              ),
            ),
            //Expanded(child: SizedBox()),
            Divider(
              color: Colors.grey,
            ),
            Image.asset(
              'assets/imagenes/logo.png',
              width: 130,
            )
          ],
        ),
      ),
      body: LayoutBuilder(builder: (context, constraint) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [

              Expanded(
                  child: Container(
                padding: const EdgeInsets.only(top: 5),
                child: Stack(
                  alignment: Alignment.center,
                  fit: mostrarMapa ? StackFit.expand : StackFit.passthrough,
                  children: [
                    ListView.builder(
                        itemCount: listaMaterias().length,
                        itemBuilder: (context, index) {
                          InformacionUsuario _info = listaMaterias()[index];
                          return Card(
                            child: ListTile(
                              onTap: () {
                                dataSalonPosition().then((value) {
                                  value.forEach((element) {
                                    if (element['ESCENARIO_ID'] ==
                                        _info.idSalon) {
                                      setState(() {
                                        mostrarMapa = true;
                                      });
                                      if (kIsWeb) {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) =>
                                                    MapaSalonesUbicacion(
                                                      name: element[
                                                                  'DESCRIPCIONLUGAR'] ==
                                                              null
                                                          ? element[
                                                              'ESCENARIO_ID']
                                                          : element[
                                                              'DESCRIPCIONLUGAR'],
                                                      long: double.parse(
                                                          element['LONG']),
                                                      lat: double.parse(
                                                          element['LAT']),
                                                      piso: element['PISO'],
                                                    )))
                                            .then((value) {
                                          setState(() {
                                            mostrarMapa = false;
                                          });
                                        });
                                      } else if (Platform.isLinux) {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) =>
                                                    MapaSalonesUbicacion(
                                                      name: element[
                                                                  'DESCRIPCIONLUGAR'] ==
                                                              null
                                                          ? element[
                                                              'ESCENARIO_ID']
                                                          : element[
                                                              'DESCRIPCIONLUGAR'],
                                                      long: double.parse(
                                                          element['LONG']),
                                                      lat: double.parse(
                                                          element['LAT']),
                                                      piso: element['PISO'],
                                                    )))
                                            .then((value) {
                                          setState(() {
                                            mostrarMapa = false;
                                          });
                                        });
                                      } else {
                                        print('android');
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) =>
                                                    MapaSalonesUbicacion(
                                                      name: element[
                                                                  'DESCRIPCIONLUGAR'] ==
                                                              null
                                                          ? element[
                                                              'ESCENARIO_ID']
                                                          : element[
                                                              'DESCRIPCIONLUGAR'],
                                                      long: double.parse(
                                                          element['LONG']),
                                                      lat: double.parse(
                                                          element['LAT']),
                                                      piso: element['PISO'],
                                                    )))
                                            .then((value) {
                                          setState(() {
                                            mostrarMapa = false;
                                          });
                                        });
                                        // Posicion().getCurrentLocation().then((value) {
                                        //   print('lolo');
                                        //   print('lolo: ${value.longitude}');
                                        //   Navigator.of(context).push(MaterialPageRoute(
                                        //       builder: (context) => MapaSalonesUbicacion(
                                        //         long: double.parse(element['LONG']),
                                        //         lat: double.parse(element['LAT']),
                                        //         piso: element['PISO'],
                                        //       )));
                                        // }).whenComplete(() {
                                        //   setState(() {
                                        //     mostrarMapa = false;
                                        //   });
                                        // });
                                      }
                                    }
                                  });
                                }).catchError((error) {
                                  print(error);
                                });
                              },
                              title: Text(
                                _info.nombreMateria,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              isThreeLine: true,
                              subtitle: Text(
                                '${_info.salon}\n${_info.horaInicial} - ${_info.horaFinal}\n${PaginaInicial._mostrarDatos.dias[_info.dia]}',
                                style: TextStyle(),
                              ),
                            ),
                          );
                        }),
                    mostrarMapa
                        ? Container(
                            width: 0,
                            height: 0,
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : SizedBox()
                  ],
                ),
              ))
            ],
          ),
        );
      }),
    );
  }
}
