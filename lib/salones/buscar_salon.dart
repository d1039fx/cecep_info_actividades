import 'dart:convert';

import 'package:claseinfoactividades/clases/buscar_datos_salon.dart';
import 'package:claseinfoactividades/clases/mostrar_imagenes_lugares.dart';
import 'package:claseinfoactividades/clases/mostrar_info_salon_model.dart';
import 'package:claseinfoactividades/clases/posicion.dart';
import 'package:claseinfoactividades/salones/ver_salon.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:io';

import 'mapa_salones_ubicacion.dart';

class BuscarSalon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buscar Salon'),
      ),
      body: BuscarSalonContenido(),
    );
  }
}

class BuscarSalonContenido extends StatefulWidget {
  @override
  _BuscarSalonContenidoState createState() => _BuscarSalonContenidoState();
}

class _BuscarSalonContenidoState extends State<BuscarSalonContenido>
    with MostrarImagenesLugares {
  TextEditingController _salonId = TextEditingController();
  BuscarDatosSalon _buscarSalon = BuscarDatosSalon();
  Box<List<Map<String, dynamic>>> _datos = Hive.box('escenario');
  List<InfoSalon> _listaSalones = [];
  bool buscar = false;
  bool mostrarMapa = false;
  String urlImages = 'https://moodle.cecep.edu.co/autoevaluacion/imagenes';

  List<String> data = [];

  List<Map<String, dynamic>> urlImageLabsList = [
    {
      'id': 7156,
      'url':
          'https://firebasestorage.googleapis.com/v0/b/taller-c737a.appspot.com/o/lab_fisica_quimica%2FLabFyQ3-min.jpg?alt=media&token=158eab16-1983-453b-a8bc-c92a0b524cec'
    },
    {
      'id': 7157,
      'url':
          'https://firebasestorage.googleapis.com/v0/b/taller-c737a.appspot.com/o/lab_fisica_quimica%2FLabFyQ3-min.jpg?alt=media&token=158eab16-1983-453b-a8bc-c92a0b524cec'
    },
    {
      'id': 7158,
      'url':
          'https://firebasestorage.googleapis.com/v0/b/taller-c737a.appspot.com/o/lab_analogo%2Flab_Analogo3-min.JPG?alt=media&token=808229fc-be64-488c-a8a2-0ab46f275322'
    },
    {
      'id': 7159,
      'url':
          'https://firebasestorage.googleapis.com/v0/b/taller-c737a.appspot.com/o/lab_electronica_digital%2Fd7-min.jpg?alt=media&token=3794e8be-e6d1-4b69-a255-168e54b091cb'
    },
    {
      'id': 7080,
      'url':
          'https://firebasestorage.googleapis.com/v0/b/taller-c737a.appspot.com/o/lab_calidad%2Ff6new%20(1%20de%201).jpg?alt=media&token=b0e816e6-24b7-4dfd-a3f3-698e5162383c'
    }
  ];

  String urlImageLab({required int idLab}) {
    String url = '$urlImages/7151/f2.jpg';
    obtenerImagenesUrl(idLugar: idLab).then((value) => print(value));
    //print(url);

    // int pos = urlImageLabsList.indexWhere((element) => element['id'] == idLab);
    // if (pos >= 0) {
    //   url = urlImageLabsList[pos]['url'];
    // } else {
    //   url = '';
    // }
    return url;
  }

  Padding _textoOrganizado({required String nombre, required String contenido}) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text.rich(TextSpan(
          text: nombre,
          style: TextStyle(fontWeight: FontWeight.bold),
          children: [
            TextSpan(
                text: contenido,
                style: TextStyle(fontWeight: FontWeight.normal))
          ])),
    );
  }

  Future<List<dynamic>> dataSalonPosition() async {
    String data =
        await rootBundle.loadString('assets/ubicaciones/escenarios.txt');
    return json.decode(data);
  }

  Future<void> _mostraInfoSalon({required int index}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          titlePadding: const EdgeInsets.all(0),
          shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: Colors.transparent)),
          title: Container(
              alignment: Alignment.center,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.red,
                  border: Border(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Text(
                'Información Salón',
                style: TextStyle(color: Colors.white),
              )),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                _textoOrganizado(
                    nombre: 'Capacidad: ',
                    contenido:
                        _listaSalones[index].capacidadEscenario.toString()),
                _textoOrganizado(
                    contenido: _listaSalones[index].descEscenario,
                    nombre: 'Descripcion: '),
                _textoOrganizado(
                    contenido: _listaSalones[index].descEdificio,
                    nombre: 'Edificio: '),
                _textoOrganizado(
                    contenido: _listaSalones[index].estadoEscenario == 'ACT'
                        ? 'Activo'
                        : _listaSalones[index].estadoEscenario,
                    nombre: 'Estado: ')
              ],
            ),
          ),
          actions: <Widget>[
            TextButton.icon(
              icon: Icon(
                Icons.clear,
                color: Colors.red,
              ),
              label: Text('Salir'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton.icon(
              icon: Icon(
                Icons.location_on,
                color: Colors.green,
              ),
              label: Text(
                'Encontrar',
                style: TextStyle(color: Colors.green),
              ),
              onPressed: () {
                dataSalonPosition().then((value) {
                  value.forEach((element) {
                    if (element['ESCENARIO_ID'] ==
                        _listaSalones[index].idEscenario.toString()) {
                      setState(() {
                        mostrarMapa = true;
                      });
                      if (kIsWeb) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => MapaSalonesUbicacion(
                                  name: element[
                                  'DESCRIPCIONLUGAR'] ==
                                      null
                                      ? element[
                                  'ESCENARIO_ID']
                                      : element[
                                  'DESCRIPCIONLUGAR'],
                                      long: double.parse(element['LONG']),
                                      lat: double.parse(element['LAT']),
                                      piso: element['PISO'], key: Key('value'),
                                    )))
                            .then((value) {
                          setState(() {
                            mostrarMapa = false;
                          });
                        });
                      } else if (Platform.isLinux) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => MapaSalonesUbicacion(
                                  name: element[
                                  'DESCRIPCIONLUGAR'] ==
                                      null
                                      ? element[
                                  'ESCENARIO_ID']
                                      : element[
                                  'DESCRIPCIONLUGAR'],
                                      long: double.parse(element['LONG']),
                                      lat: double.parse(element['LAT']),
                                      piso: element['PISO'], key: Key('value'),
                                    )))
                            .then((value) {
                          setState(() {
                            mostrarMapa = false;
                          });
                        });
                      } else {
                        Posicion().getCurrentLocation().then((value) {
                          Navigator.of(context)
                              .push(MaterialPageRoute(
                                  builder: (context) => MapaSalonesUbicacion(
                                    name: element[
                                    'DESCRIPCIONLUGAR'] ==
                                        null
                                        ? element[
                                    'ESCENARIO_ID']
                                        : element[
                                    'DESCRIPCIONLUGAR'],
                                        long: double.parse(element['LONG']),
                                        lat: double.parse(element['LAT']),
                                        piso: element['PISO'], key: Key('value'),
                                      )))
                              .then((value) => Navigator.of(context).pop());
                        }).whenComplete(() {
                          setState(() {
                            mostrarMapa = false;
                          });
                        });
                      }
                    }
                  });
                });
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //entrada para ingresar datos
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: _salonId,
            onChanged: (valor) {
              if (valor.isEmpty) {
                setState(() {
                  _listaSalones.clear();
                });
              } else {
                setState(() {
                  _listaSalones = _buscarSalon.buscarSalon(
                      listaEscenarios: _datos.get('lista_escenarios')!,
                      escenario: valor[0].toUpperCase() + valor.substring(1));
                });
              }
            },
            decoration: InputDecoration(
                labelText: 'Ingrese texto',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20))),
          ),
        ),
        //lista de datos
        Expanded(
            child: ListView.builder(
                itemCount: _listaSalones.length,
                itemBuilder: (context, index) {
                  return Container(
                    color: index % 2 == 0 ? Colors.grey[200] : Colors.white,
                    child: ListTile(
                      title: Text(_listaSalones[index].descEscenario),
                      subtitle: Text(_listaSalones[index].descEdificio),
                      trailing: kIsWeb ? null : _listaSalones[index].idTipoEscenario != 3
                          ? Container(
                              width: 45,
                            )
                          : FutureBuilder<String>(
                              future: obtenerImagenesUrl(
                                  idLugar: _listaSalones[index].idEscenario),
                              builder: (context, snapshot) {
                                if (snapshot.hasError)
                                  return Text(snapshot.error.toString());
                                if (snapshot.hasData) {
                                  switch (snapshot.data) {
                                    case '404':
                                      return SizedBox();

                                    default:
                                      return IconButton(
                                          onPressed: () => Navigator.of(context)
                                              .push(MaterialPageRoute(
                                                  builder: (context) =>
                                                      VerSalon(key: Key('value'),
                                                        urlLabImage:
                                                            snapshot.data!,
                                                      ))),
                                          icon: Icon(Icons.remove_red_eye));
                                  }
                                }
                                return SizedBox(
                                  width: 45,
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                );
                              }),
                      // : urlImageLab(idLab: _listaSalones[index].idEscenario)
                      //         .isEmpty
                      //     ? Container(
                      //         width: 45,
                      //       )
                      //     : IconButton(
                      //         icon: Icon(Icons.remove_red_eye),
                      //         onPressed: () => Navigator.of(context)
                      //             .push(MaterialPageRoute(
                      //                 builder: (context) => VerSalon(
                      //                       urlLabImage: urlImageLab(
                      //                           idLab: _listaSalones[index]
                      //                               .idEscenario),
                      //                     )))),
                      onTap: () {
                        _mostraInfoSalon(index: index);
                      },
                    ),
                  );
                }))
      ],
    );
  }
}
