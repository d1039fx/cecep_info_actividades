import 'package:claseinfoactividades/clases/posicion.dart';
import 'package:flutter/material.dart';
import 'package:panorama/panorama.dart';

class VerSalon extends StatefulWidget {
  final String urlLabImage;

  const VerSalon({required Key key, required this.urlLabImage}) : super(key: key);


  @override
  _VerSalonState createState() => _VerSalonState();
}

class _VerSalonState extends State<VerSalon> with Posicion {
  @override
  void initState() {
    // TODO: implement initState
    realTimePosition();
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Imagen Salon'),
      ),

      // body: StreamBuilder<Position>(stream: realTimePosition().,builder: (context, snapshot){
      //   return Text(snapshot.data.longitude.toString());
      // }),
      body: Panorama(
        // sensorControl: SensorControl.values[SensorControl.Orientation.index],
        interactive: true,
        sensitivity: 1.5,
        onViewChanged: (x, y, z){
          print(x);
        },
        child: Image.network(
          widget.urlLabImage,
          fit: BoxFit.fill,
          loadingBuilder: (context, child, loadingProgress) {
            if (loadingProgress == null) return child;
            print(loadingProgress.cumulativeBytesLoaded);
            return Center(
              child: CircularProgressIndicator(
                value: loadingProgress.expectedTotalBytes != null
                    ? loadingProgress.cumulativeBytesLoaded /
                        loadingProgress.expectedTotalBytes!
                    : null,
              ),
            );
          },
        ),
      ),
      // body: Image.network(
      //   widget.urlLabImage,
      //   fit: BoxFit.fill,
      //   loadingBuilder: (context, child, loadingProgress) {
      //     if (loadingProgress == null) return child;
      //     return Center(
      //       child: CircularProgressIndicator(
      //         value: loadingProgress.expectedTotalBytes != null
      //             ? loadingProgress.cumulativeBytesLoaded /
      //                 loadingProgress.expectedTotalBytes
      //             : null,
      //       ),
      //     );
      //   },
      // ),
    );
  }
}
