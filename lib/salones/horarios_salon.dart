import 'dart:async';
import 'dart:io';

import 'package:claseinfoactividades/clases/info_salon_qr_model.dart';
import 'package:claseinfoactividades/clases/mostrar_datos.dart';
import 'package:claseinfoactividades/clases/obtener_datos_salon_qr.dart';
import 'package:claseinfoactividades/clases/posicion.dart';
import 'package:claseinfoactividades/clases/reloj_procesos.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'mapa_salones_ubicacion.dart';

class HorariosSalon extends StatefulWidget {
  final String idSalon, tituloEspacio;

  const HorariosSalon({ Key? key, required this.idSalon, required this.tituloEspacio})
      : super(key: key);

  static ObtenerDatosSalonQr salonInfoQr = ObtenerDatosSalonQr();

  @override
  _HorariosSalonState createState() => _HorariosSalonState();
}

class _HorariosSalonState extends State<HorariosSalon> with MostrarDatos {
  late Timer timerClock;
  late RelojProcesos relojProcesos;
  bool mostrarMapa = false;

  @override
  void initState() {
    // TODO: implement initState
    relojProcesos = Get.put(RelojProcesos());
    timerClock = relojProcesos.timeOfDayResultSet(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    timerClock.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.tituloEspacio),
        actions: [
          IconButton(
              onPressed: () {
                print(widget.idSalon);
                dataSalonPosition().then((value) {
                  if(value.where((element) => element['ESCENARIO_ID'] == widget.idSalon).isEmpty){
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('No hay Ubicación')));
                  }

                  value.forEach((element) {
                    if (element['ESCENARIO_ID'] == widget.idSalon) {
                      setState(() {
                        mostrarMapa = true;
                      });

                      if (kIsWeb) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => MapaSalonesUbicacion(
                                      name: element['DESCRIPCIONLUGAR'] == null
                                          ? element['ESCENARIO_ID']
                                          : element['DESCRIPCIONLUGAR'],
                                      long: double.parse(element['LONG']),
                                      lat: double.parse(element['LAT']),
                                      piso: element['PISO'], key: Key('value'),
                                    )))
                            .then((value) {
                          setState(() {
                            mostrarMapa = false;
                          });
                        });
                      } else if (Platform.isLinux) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => MapaSalonesUbicacion(key: Key('value'),
                                      name: element['DESCRIPCIONLUGAR'] == null
                                          ? element['ESCENARIO_ID']
                                          : element['DESCRIPCIONLUGAR'],
                                      long: double.parse(element['LONG']),
                                      lat: double.parse(element['LAT']),
                                      piso: element['PISO'],
                                    )))
                            .then((value) {
                          setState(() {
                            mostrarMapa = false;
                          });
                        });
                      } else {
                        Posicion().getCurrentLocation().then((value) {
                          print(value);
                          Navigator.of(context)
                              .push(MaterialPageRoute(
                                  builder: (context) => MapaSalonesUbicacion(key: Key('value'),
                                        name:
                                            element['DESCRIPCIONLUGAR'] == null
                                                ? element['ESCENARIO_ID']
                                                : element['DESCRIPCIONLUGAR'],
                                        long: double.parse(element['LONG']),
                                        lat: double.parse(element['LAT']),
                                        piso: element['PISO'],
                                      )))
                               ;
                        }).whenComplete(() {
                          setState(() {
                            mostrarMapa = false;
                          });
                        }).catchError((error) {
                          print(error.toString());
                        });
                      }
                    }
                  });
                });
              },
              icon: Icon(Icons.map))
        ],
      ),
      body: FutureBuilder<List<InfoSalonQrModel>>(
        future:
            HorariosSalon.salonInfoQr.obtenerDatosQr(codigo: widget.idSalon),
        builder: (context, snapshot) {
          if (snapshot.hasError) return Text(snapshot.error.toString());
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          return GetBuilder<RelojProcesos>(builder: (data) {
            if (data.isClosed) {
              return CircularProgressIndicator();
            }
            return ListView.separated(
                itemBuilder: (context, index) {
                  List<InfoSalonQrModel> listData = data.listSalonState(
                      context: context, infoSalonQrModel: snapshot.data!)[index];
                  return index == 1
                      ? listData.isEmpty
                          ? Card(
                              child: Container(
                                  color: data.stateColorClass(index),
                                  constraints:
                                      BoxConstraints.expand(height: 40),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Hora Actual\n${data.timeOfDayResultGet}',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  )),
                            )
                          : Card(
                              child: Column(
                                children: [
                                  Container(
                                    constraints:
                                        BoxConstraints.expand(height: 40),
                                    alignment: Alignment.center,
                                    color: data.stateColorClass(index),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          data.stateClass(index),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          'Hora Actual\n${data.timeOfDayResultGet}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        )
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: listData
                                        .map<Widget>((e) => ListTile(
                                              isThreeLine: true,
                                              title: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Expanded(
                                                      flex: 2,
                                                      child:
                                                          Text(e.descMateria)),
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Text(e.horaInicio),
                                                      Text(e.horaFinal),
                                                    ],
                                                  ))
                                                ],
                                              ),
                                              subtitle: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(e.descPlan),
                                                  Text(e.docente),
                                                  Text(e.idGrupo),
                                                ],
                                              ),
                                            ))
                                        .toList(),
                                  ),
                                ],
                              ),
                            )
                      : listData.isEmpty
                          ? SizedBox()
                          : Card(
                              child: Column(
                                children: [
                                  Container(
                                    color: data.stateColorClass(index),
                                    constraints:
                                        BoxConstraints.expand(height: 40),
                                    alignment: Alignment.center,
                                    child: Text(
                                      data.stateClass(index),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Column(
                                    children: listData
                                        .map<Widget>((e) => Column(
                                              children: [
                                                ListTile(
                                                  title: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Expanded(
                                                        flex: 2,
                                                        child:
                                                            Text(e.descMateria),
                                                      ),
                                                      Expanded(
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceAround,
                                                          children: [
                                                            Text(e.horaInicio),
                                                            Text(e.horaFinal),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  subtitle: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .stretch,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(e.descPlan),
                                                      Text(e.docente),
                                                      Text(e.idGrupo),
                                                    ],
                                                  ),
                                                ),
                                                listData.indexOf(e) ==
                                                        listData.length - 1
                                                    ? SizedBox()
                                                    : Divider()
                                              ],
                                            ))
                                        .toList(),
                                  ),
                                ],
                              ),
                            );
                },
                separatorBuilder: (context, index) {
                  return SizedBox();
                },
                itemCount: data
                    .listSalonState(
                        context: context, infoSalonQrModel: snapshot.data!)
                    .length);
          });
        },
      ),
    );
  }
}
