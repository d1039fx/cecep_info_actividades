import 'dart:async';


import 'package:claseinfoactividades/clases/info_salon_qr_model.dart';
import 'package:claseinfoactividades/clases/obtener_datos_salon_qr.dart';
import 'package:claseinfoactividades/clases/reloj_procesos.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:qr_code_scanner/qr_code_scanner.dart';

class InfoSalones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Información de Salones'),
      ),
      body: MostraInfoSalon(),
    );
  }
}

class MostraInfoSalon extends StatefulWidget {
  @override
  _MostraInfoSalonState createState() => _MostraInfoSalonState();
}

class _MostraInfoSalonState extends State<MostraInfoSalon> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  String qrText = '';
  late QRViewController controller;
  ObtenerDatosSalonQr salonInfoQr = ObtenerDatosSalonQr();
  RelojProcesos relojProcesos = Get.put(RelojProcesos());
  late Timer timerClock;

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((event) {
      setState(() {
        qrText = event.code!;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    timerClock = relojProcesos.timeOfDayResultSet(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    timerClock.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return qrText.isEmpty
        ? Stack(
            alignment: Alignment.bottomCenter,
            children: [
              QRView(key: qrKey, onQRViewCreated: _onQRViewCreated),
              Container(
                constraints: BoxConstraints.expand(height: 50),
                child: Center(
                    child: Text(
                  'Leer codigo Qr',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    shadows: <Shadow>[
                      Shadow(
                        offset: Offset(4.0, 4.0),
                        blurRadius: 3.0,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                      Shadow(
                        offset: Offset(4.0, 4.0),
                        blurRadius: 8.0,
                        color: Color.fromARGB(125, 0, 0, 0),
                      ),
                    ],
                  ),
                )),
              )
            ],
          )
        : Column(
            children: [
              Expanded(
                child: FutureBuilder<List<InfoSalonQrModel>>(
                  future: salonInfoQr.obtenerDatosQr(codigo: qrText),
                  builder: (context, snapshot) {
                    if (snapshot.hasError)
                      return Text(snapshot.error.toString());
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    return GetBuilder<RelojProcesos>(builder: (data) {
                      return ListView.separated(
                          itemBuilder: (context, index) {
                            List<InfoSalonQrModel> listData =
                                data.listSalonState(
                                    context: context,
                                    infoSalonQrModel: snapshot.data!)[index];
                            return index == 1
                                ? listData.isEmpty
                                    ? Card(
                                        child: Container(
                                            color: data.stateColorClass(index),
                                            constraints: BoxConstraints.expand(
                                                height: 40),
                                            alignment: Alignment.center,
                                            child: Text(
                                              'Hora Actual\n${data.timeOfDayResultGet}',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                              textAlign: TextAlign.center,
                                            )),
                                      )
                                    : Card(
                                        child: Column(
                                          children: [
                                            Container(
                                              constraints:
                                                  BoxConstraints.expand(
                                                      height: 40),
                                              alignment: Alignment.center,
                                              color:
                                                  data.stateColorClass(index),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Text(
                                                    data.stateClass(index),
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    'Hora Actual\n${data.timeOfDayResultGet}',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign: TextAlign.center,
                                                  )
                                                ],
                                              ),
                                            ),
                                            Column(
                                              children: listData
                                                  .map<Widget>((e) => ListTile(
                                                        isThreeLine: true,
                                                        title: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceAround,
                                                          children: [
                                                            Expanded(
                                                                flex: 2,
                                                                child: Text(e
                                                                    .descMateria)),
                                                            Expanded(
                                                                child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceAround,
                                                              children: [
                                                                Text(e
                                                                    .horaInicio),
                                                                Text(e
                                                                    .horaFinal),
                                                              ],
                                                            ))
                                                          ],
                                                        ),
                                                        subtitle: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .stretch,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Text(e.descPlan),
                                                            Text(e.docente),
                                                            Text(e.idGrupo),
                                                          ],
                                                        ),
                                                      ))
                                                  .toList(),
                                            ),
                                          ],
                                        ),
                                      )
                                : listData.isEmpty
                                    ? SizedBox()
                                    : Card(
                                        child: Column(
                                          children: [
                                            Container(
                                              color:
                                                  data.stateColorClass(index),
                                              constraints:
                                                  BoxConstraints.expand(
                                                      height: 40),
                                              alignment: Alignment.center,
                                              child: Text(
                                                data.stateClass(index),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Column(
                                              children: listData
                                                  .map<Widget>((e) => Column(
                                                        children: [
                                                          ListTile(
                                                            title: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceAround,
                                                              children: [
                                                                Expanded(
                                                                  flex: 2,
                                                                  child: Text(e
                                                                      .descMateria),
                                                                ),
                                                                Expanded(
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceAround,
                                                                    children: [
                                                                      Text(e
                                                                          .horaInicio),
                                                                      Text(e
                                                                          .horaFinal),
                                                                    ],
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                            subtitle: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .stretch,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                    e.descPlan),
                                                                Text(e.docente),
                                                                Text(e.idGrupo),
                                                              ],
                                                            ),
                                                          ),
                                                          listData.indexOf(e) ==
                                                                  listData.length -
                                                                      1
                                                              ? SizedBox()
                                                              : Divider()
                                                        ],
                                                      ))
                                                  .toList(),
                                            ),
                                          ],
                                        ),
                                      );
                          },
                          separatorBuilder: (context, index) {
                            return SizedBox();
                          },
                          itemCount: data
                              .listSalonState(
                                  context: context,
                                  infoSalonQrModel: snapshot.data!)
                              .length);
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: () {
                      setState(() {
                        qrText = '';
                      });
                    },
                    icon: Icon(
                      Icons.qr_code_scanner,
                      size: 50,
                    )),
              )
            ],
          );
  }
}
