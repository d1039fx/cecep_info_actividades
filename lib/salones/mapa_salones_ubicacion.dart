import 'dart:ui';

import 'package:claseinfoactividades/clases/constantes.dart';
import 'package:claseinfoactividades/widgets/info_oficina.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'dart:async';


class MapaSalonesUbicacion extends StatelessWidget {
  final double lat, long;
  final String piso, name;

  const MapaSalonesUbicacion(
      { Key? key, required this.lat, required this.long, required this.piso, required this.name})
      : super(key: key);

  static double _sigmaX = 5.0; // from 0-10
  static double _sigmaY = 5.0; // from 0-10
  static double _opacity = 0.7; // from 0-1.0
  static Constantes _constantes = Constantes();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(name),
      ),
      body: !_constantes.oficinas.contains(name)
          ? Stack(
        children: [
          MapTest(
            lat: lat,
            long: long,
            piso: piso, key: Key('value'),
          ),
          Positioned(
            left: 20,
            top: 20,
            child: CircleAvatar(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    piso,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Piso',
                    style: TextStyle(fontSize: 10),
                  )
                ],
              ),
            ),
          ),
        ],
      )
          : Stack(
              children: [
                MapTest(
                  lat: lat,
                  long: long,
                  piso: piso, key: Key('value'),
                ),
                Positioned(
                  left: 20,
                  top: 20,
                  child: CircleAvatar(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          piso,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'Piso',
                          style: TextStyle(fontSize: 10),
                        )
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(0),
                    child: BackdropFilter(
                      filter:
                          ImageFilter.blur(sigmaX: _sigmaX, sigmaY: _sigmaY),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                              top: BorderSide(
                                  color: Colors.black.withOpacity(0.1))),
                          color: Colors.white.withOpacity(_opacity),
                        ),
                        child: InfoOficina(
                          name: name,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}

class MapTest extends StatefulWidget {
  final double lat, long;
  final String piso;

  const MapTest({required Key key, required this.lat, required this.long, required this.piso}) : super(key: key);

  @override
  _MapTestState createState() => _MapTestState();
}

class _MapTestState extends State<MapTest> {
  late Timer _timer;
  int _markerIndex = 0;

  List<Marker> _markers = [
    Marker(
      width: 20.0,
      height: 20.0,
      point: LatLng(51.5, -0.09),
      builder: (ctx) => Container(
        child: FlutterLogo(),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      point: LatLng(53.3498, -6.2603),
      builder: (ctx) => Container(
        child: FlutterLogo(),
      ),
    ),
    Marker(
      width: 20.0,
      height: 20.0,
      point: LatLng(48.8566, 2.3522),
      builder: (ctx) => Container(
        child: FlutterLogo(),
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (_) {
      setState(() {
        _markerIndex = (_markerIndex + 1) % _markers.length;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return new FlutterMap(
      options: new MapOptions(
          //interactive: true,
          center: new LatLng(3.429358, -76.534936),
          minZoom: 12.0,
          maxZoom: 21.0,
          onTap: (pos, latlong) {
            print(latlong.latitude);
            print(latlong.longitude);
          },
          zoom: 19.0,
          onPositionChanged: (pos, map) {
            //print(pos.center.latitude);
          }),
      children: [
        TileLayer(
          tileProvider: AssetTileProvider(),
          minZoom: 18,
          maxZoom: 21.0,
          urlTemplate: widget.piso == '1'
              ? 'assets/mapa/{z}/{x}/{y}.png'
              : 'assets/mapa2p/{z}/{x}/{y}.png',
        ),
        MarkerLayer(
          markers: [
            Marker(
              anchorPos: AnchorPos.align(AnchorAlign.center),
              width: 60.0,
              height: 60.0,
              point: new LatLng(widget.lat, widget.long),
              builder: (ctx) => Icon(
                Icons.radio_button_checked,
                size: 30,
                color: Color.fromRGBO(255, 0, 0, 1),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
