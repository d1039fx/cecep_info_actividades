import 'package:flutter/material.dart';

class InfoSalonesBusqueda with ChangeNotifier{

  bool _tipoBusqueda = false;

  bool get tipoBusquedaGet => _tipoBusqueda;

  set tipoBusquedaSet(bool value) {
    _tipoBusqueda = value;
    notifyListeners();
  }
}