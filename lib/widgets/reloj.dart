import 'package:claseinfoactividades/clases/reloj_procesos.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Reloj extends StatefulWidget {
  const Reloj({Key? key}) : super(key: key);

  @override
  _RelojState createState() => _RelojState();
}

class _RelojState extends State<Reloj> {
  String time = '';
  RelojProcesos relojProcesos = Get.put(RelojProcesos());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RelojProcesos>(builder: (data) {
      data.timeOfDayResultSet(context);
      return Container(
        child: Text(data.timeOfDayResultGet),
      );
    });
  }
}
