import 'package:claseinfoactividades/clases/constantes.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoOficina extends StatefulWidget {
  final String name;

  const InfoOficina({Key? key, required this.name}) : super(key: key);

  @override
  State<InfoOficina> createState() => _InfoOficinaState();
}

class _InfoOficinaState extends State<InfoOficina> with Constantes {
  bool selected = false;
  bool phone = false;
  List<String> exts = [];

  void _launchURL(String services) async => await canLaunch(services)
      ? await launch(services)
      : throw 'Could not launch';

  ListView listaExts() {
    Map<String, dynamic> lista = extencionesOficinas
        .singleWhere((element) => element['nombre'] == widget.name);
    exts = lista['ext'];
    return ListView(
      scrollDirection: Axis.horizontal,
      children: exts
          .map<Padding>((e) => Padding(
                padding: const EdgeInsets.only(right: 8, top: 8),
                child: Text(
                  e,
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
              ))
          .toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, size) {
      return SizedBox(
        height: 100,
        child: Stack(
          alignment: Alignment.center,
          fit: StackFit.expand,
          children: [
            AnimatedPositioned(
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
              top: 15.0,
              left: phone ? -size.maxWidth : 20,
              child: TextButton(
                onPressed: () {
                  setState(() {
                    selected = !selected;
                  });
                },
                child: Icon(
                  Icons.date_range,
                  color: Colors.yellow[900],
                  size: 50,
                ),
              ),
            ),
            AnimatedPositioned(
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
              top: 0,
              right: selected ? size.maxWidth - 400 : -size.maxWidth,
              child: SizedBox(
                width: 300,
                child: ListTile(
                  isThreeLine: true,
                  title: Text('Horarios'),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text('Lunes a Viernes de 8:00 am a 6:00 pm'),
                      Text('Sabado hasta la 1:00 pm'),
                      Text('Domingos y Festivos no se atiende'),
                    ],
                  ),
                ),
              ),
            ),
            AnimatedPositioned(
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
              top: selected ? 100.0 : 25.0,
              left: phone ? 20 : size.maxWidth - (size.maxWidth / 2) - 30,
              child: TextButton(
                onPressed: () {
                  setState(() {
                    phone = !phone;
                  });
                },
                child: CircleAvatar(
                  backgroundColor: Colors.green,
                  child: Icon(
                    Icons.phone,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            AnimatedPositioned(
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
              top: 0,
              right: phone ? size.maxWidth - 400 : -size.maxWidth,
              child: SizedBox(
                width: 300,
                height: size.maxHeight,
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Container(
                        color: Colors.grey,
                        child: ListTile(
                          minVerticalPadding: 18,
                          isThreeLine: true,
                          title: Text(
                            'Extensión',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: listaExts(),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        color: Colors.green,
                        padding: const EdgeInsets.symmetric(vertical: 28),
                        child: InkWell(
                          onTap: ()  => _launchURL('tel:6023828282;${exts[0]}'),
                          child: SizedBox(
                            height: size.maxHeight,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.phone,
                                  color: Colors.white,
                                ),
                                Center(
                                    child: Text(
                                  'Llamar',
                                  style: TextStyle(color: Colors.white),
                                ))
                              ],
                            ),
                          ),
                          // style: TextButton.styleFrom(
                          //   backgroundColor: Colors.red,
                          //   padding: const EdgeInsets.symmetric(vertical: 25),
                          // ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            AnimatedPositioned(
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
              top: selected ? 100.0 : 25.0,
              right: phone ? -size.maxWidth : 20,
              child: TextButton(
                onPressed: () => _launchURL('https://www.cecep.edu.co/index.php?option=com_wrapper&view=wrapper&Itemid=1417'),
                child: CircleAvatar(
                  backgroundColor: Colors.blue,
                  child: Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
