import 'package:claseinfoactividades/clases/mostrar_datos.dart';
import 'package:claseinfoactividades/salones/mapa_salones_ubicacion.dart';
import 'package:flutter/material.dart';

class DrawerWidgets with MostrarDatos {
  final BuildContext context;

  DrawerWidgets(this.context);

  Align oficina({required String nameOficina, required String lat, required String long, required String piso}) {
    return Align(
      child: TextButton(
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => MapaSalonesUbicacion(
                  name: nameOficina,
                  long: double.parse(long),
                  lat: double.parse(lat),
                  piso: piso,
                ))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              nameOficina,
              style: TextStyle(color: Colors.grey[600]),
            ),
            Icon(
              Icons.next_plan,
              color: Colors.grey[600],
            )
          ],
        ),
        style: TextButton.styleFrom(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(vertical: 20)),
      ),
    );
  }

  FutureBuilder<List<Align>> officeList({required List<String> seccion}) {
    Future<List<Align>> oficinaLista = dataSalonPosition().then((value) {
      List<Align> listData = [];
      value.forEach((element) {
        if (element['DESCRIPCIONLUGAR'] == null) {
          seccion.forEach((seccion) {
            if (seccion == element['ESCENARIO_ID']) {
              listData.add(oficina(
                  long: element['LONG'],
                  piso: element['PISO'],
                  lat: element['LAT'],
                  nameOficina: element['ESCENARIO_ID']));
            }
          });
        }
      });
      return listData;
    });
    return FutureBuilder<List<Align>>(
        future: oficinaLista,
        builder: (context, snapshot) {
          if (snapshot.hasError)
            return Center(
              child: Text(snapshot.error.toString()),
            );
          if (snapshot.hasData) {
            return ListView(
              padding: const EdgeInsets.only(top: 0),
              children: snapshot.data!,
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  Padding textDrawer(
      {required String textData, required double fontSize, required FontWeight fontWeight}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 1),
      child: Text(
        textData,
        style: TextStyle(fontSize: fontSize, fontWeight: fontWeight),
        textAlign: TextAlign.start,
      ),
    );
  }
}
