part of 'grades_presenter_bloc.dart';

abstract class GradesPresenterEvent extends Equatable {
  const GradesPresenterEvent();
}

class ExpandedBehavior extends GradesPresenterEvent{

  final bool isExpanded;
  final int index;

  ExpandedBehavior({required this.isExpanded, required this.index});

  @override
  // TODO: implement props
  List<Object?> get props => [isExpanded, index];
}
