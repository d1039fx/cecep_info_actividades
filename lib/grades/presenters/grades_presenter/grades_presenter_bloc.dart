import 'package:bloc/bloc.dart';

import 'package:equatable/equatable.dart';

part 'grades_presenter_event.dart';
part 'grades_presenter_state.dart';

class GradesPresenterBloc
    extends Bloc<GradesPresenterEvent, GradesPresenterState> {
  GradesPresenterBloc()
      : super(GradesPresenterInitial(isExpanded: false, index: 0)) {
    on<ExpandedBehavior>(expandedBehavior);
  }
  void expandedBehavior(
      ExpandedBehavior event, Emitter<GradesPresenterState> emitter) {
    if (event.isExpanded) {
      emitter(GradesPresenterInitial(isExpanded: true, index: event.index));
    } else {
      emitter(GradesPresenterInitial(isExpanded: false, index: event.index));
    }
  }
}
