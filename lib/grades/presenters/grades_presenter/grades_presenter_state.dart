part of 'grades_presenter_bloc.dart';

abstract class GradesPresenterState extends Equatable {
  final bool isExpanded;
  final int index;
  const GradesPresenterState({required this.isExpanded, required this.index});
}

class GradesPresenterInitial extends GradesPresenterState {
  GradesPresenterInitial({required bool isExpanded, required int index})
      : super(isExpanded: isExpanded, index: index);

  @override
  List<Object> get props => [isExpanded, index];
}
