part of 'grades_data_bloc.dart';

abstract class GradesDataEvent extends Equatable {
  const GradesDataEvent();
}

class GetGradesData extends GradesDataEvent {
  final String planId, codigo, documento, materiaId, materiaDesc;

  GetGradesData(
      {required this.planId,
      required this.materiaDesc,
      required this.documento,
      required this.codigo,
      required this.materiaId});

  @override
  // TODO: implement props
  List<Object?> get props =>
      [planId, documento, codigo, materiaId, materiaDesc];
}
