part of 'grades_data_bloc.dart';

abstract class GradesDataState extends Equatable {
  final ModelDataGrades modelDataGrades;

  const GradesDataState({required this.modelDataGrades});
}

class GradesDataInitial extends GradesDataState {
  GradesDataInitial({required ModelDataGrades modelDataGrades})
      : super(modelDataGrades: modelDataGrades);

  @override
  List<Object> get props => [modelDataGrades];
}

class GradesData extends GradesDataState {
  GradesData({required ModelDataGrades modelDataGrades})
      : super(modelDataGrades: modelDataGrades);

  @override
  List<Object> get props => [modelDataGrades];
}
