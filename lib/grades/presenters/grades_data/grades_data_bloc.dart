import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:claseinfoactividades/grades/models/model_data_grades.dart';
import 'package:equatable/equatable.dart';

import '../../repository/data_grades.dart';

part 'grades_data_event.dart';
part 'grades_data_state.dart';

class GradesDataBloc extends Bloc<GradesDataEvent, GradesDataState> {
  DataGrades dataGrades = DataGrades();
  GradesDataBloc()
      : super(GradesDataInitial(
            modelDataGrades: ModelDataGrades(
                ESTUDIANTE_ID: '',
                PARCIAL_1: '',
                BIENESTAR: 0,
                PARCIAL_2: '',
                PARCIAL_3: '',
                NOTA_HABILIT: '',
                NOTA_CRVER: '',
                NOTA_DEF: '',
                GRUPO_ID: '',
                MATERIA_ID: '',
                DESC_MATERIA: ''))) {
    on<GetGradesData>(gradesGetData);
  }

  void gradesGetData(
      GetGradesData event, Emitter<GradesDataState> emitter) async {
    if (event.materiaId.isNotEmpty) {
      ModelDataGrades modelDataGrades = await dataGrades
          .dataGradesGet(
        codigo: event.codigo,
        plan: event.planId,
      )
          .then((value) {
        List dataGrades = jsonDecode(value.body);
        List<Map<String, dynamic>> data =
            dataGrades.map<Map<String, dynamic>>((e) => e).toList();
        List<ModelDataGrades> dataModelGrades = data
            .map<ModelDataGrades>((e) => ModelDataGrades.fromJson(e))
            .toList();
        return dataModelGrades[dataModelGrades
            .indexWhere((element) => element.MATERIA_ID == event.materiaId)];
      });

      emitter(GradesDataInitial(modelDataGrades: modelDataGrades));
    } else {
      emitter(GradesDataInitial(
          modelDataGrades: ModelDataGrades(
              ESTUDIANTE_ID: '',
              PARCIAL_1: '',
              BIENESTAR: 0,
              PARCIAL_2: '',
              PARCIAL_3: '',
              NOTA_HABILIT: '',
              NOTA_CRVER: '',
              NOTA_DEF: '',
              GRUPO_ID: '',
              MATERIA_ID: '',
              DESC_MATERIA: '')));
    }
  }
}
