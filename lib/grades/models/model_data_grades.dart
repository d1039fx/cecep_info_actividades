import 'package:freezed_annotation/freezed_annotation.dart';

part 'model_data_grades.g.dart';

part 'model_data_grades.freezed.dart';

@freezed
class ModelDataGrades with _$ModelDataGrades {
  const factory ModelDataGrades(
      { required String ESTUDIANTE_ID,
      required String PARCIAL_1,
      required int BIENESTAR,
      required String PARCIAL_2,
      required String PARCIAL_3,
      required String NOTA_HABILIT,
      required String NOTA_CRVER,
      required String NOTA_DEF,
      required String GRUPO_ID,
      required String MATERIA_ID,
      required String DESC_MATERIA}) = _ModelDataGrades;

  factory ModelDataGrades.fromJson(Map<String, dynamic> json) =>
      _$ModelDataGradesFromJson(json);
}
