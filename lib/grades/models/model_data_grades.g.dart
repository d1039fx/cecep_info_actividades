// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model_data_grades.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ModelDataGrades _$$_ModelDataGradesFromJson(Map<String, dynamic> json) =>
    _$_ModelDataGrades(
      ESTUDIANTE_ID: json['ESTUDIANTE_ID'] as String,
      PARCIAL_1: json['PARCIAL_1'] as String,
      BIENESTAR: json['BIENESTAR'] as int,
      PARCIAL_2: json['PARCIAL_2'] as String,
      PARCIAL_3: json['PARCIAL_3'] as String,
      NOTA_HABILIT: json['NOTA_HABILIT'] as String,
      NOTA_CRVER: json['NOTA_CRVER'] as String,
      NOTA_DEF: json['NOTA_DEF'] as String,
      GRUPO_ID: json['GRUPO_ID'] as String,
      MATERIA_ID: json['MATERIA_ID'] as String,
      DESC_MATERIA: json['DESC_MATERIA'] as String,
    );

Map<String, dynamic> _$$_ModelDataGradesToJson(_$_ModelDataGrades instance) =>
    <String, dynamic>{
      'ESTUDIANTE_ID': instance.ESTUDIANTE_ID,
      'PARCIAL_1': instance.PARCIAL_1,
      'BIENESTAR': instance.BIENESTAR,
      'PARCIAL_2': instance.PARCIAL_2,
      'PARCIAL_3': instance.PARCIAL_3,
      'NOTA_HABILIT': instance.NOTA_HABILIT,
      'NOTA_CRVER': instance.NOTA_CRVER,
      'NOTA_DEF': instance.NOTA_DEF,
      'GRUPO_ID': instance.GRUPO_ID,
      'MATERIA_ID': instance.MATERIA_ID,
      'DESC_MATERIA': instance.DESC_MATERIA,
    };
