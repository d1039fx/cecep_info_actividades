// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'model_data_grades.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ModelDataGrades _$ModelDataGradesFromJson(Map<String, dynamic> json) {
  return _ModelDataGrades.fromJson(json);
}

/// @nodoc
mixin _$ModelDataGrades {
  String get ESTUDIANTE_ID => throw _privateConstructorUsedError;
  String get PARCIAL_1 => throw _privateConstructorUsedError;
  int get BIENESTAR => throw _privateConstructorUsedError;
  String get PARCIAL_2 => throw _privateConstructorUsedError;
  String get PARCIAL_3 => throw _privateConstructorUsedError;
  String get NOTA_HABILIT => throw _privateConstructorUsedError;
  String get NOTA_CRVER => throw _privateConstructorUsedError;
  String get NOTA_DEF => throw _privateConstructorUsedError;
  String get GRUPO_ID => throw _privateConstructorUsedError;
  String get MATERIA_ID => throw _privateConstructorUsedError;
  String get DESC_MATERIA => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ModelDataGradesCopyWith<ModelDataGrades> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ModelDataGradesCopyWith<$Res> {
  factory $ModelDataGradesCopyWith(
          ModelDataGrades value, $Res Function(ModelDataGrades) then) =
      _$ModelDataGradesCopyWithImpl<$Res>;
  $Res call(
      {String ESTUDIANTE_ID,
      String PARCIAL_1,
      int BIENESTAR,
      String PARCIAL_2,
      String PARCIAL_3,
      String NOTA_HABILIT,
      String NOTA_CRVER,
      String NOTA_DEF,
      String GRUPO_ID,
      String MATERIA_ID,
      String DESC_MATERIA});
}

/// @nodoc
class _$ModelDataGradesCopyWithImpl<$Res>
    implements $ModelDataGradesCopyWith<$Res> {
  _$ModelDataGradesCopyWithImpl(this._value, this._then);

  final ModelDataGrades _value;
  // ignore: unused_field
  final $Res Function(ModelDataGrades) _then;

  @override
  $Res call({
    Object? ESTUDIANTE_ID = freezed,
    Object? PARCIAL_1 = freezed,
    Object? BIENESTAR = freezed,
    Object? PARCIAL_2 = freezed,
    Object? PARCIAL_3 = freezed,
    Object? NOTA_HABILIT = freezed,
    Object? NOTA_CRVER = freezed,
    Object? NOTA_DEF = freezed,
    Object? GRUPO_ID = freezed,
    Object? MATERIA_ID = freezed,
    Object? DESC_MATERIA = freezed,
  }) {
    return _then(_value.copyWith(
      ESTUDIANTE_ID: ESTUDIANTE_ID == freezed
          ? _value.ESTUDIANTE_ID
          : ESTUDIANTE_ID // ignore: cast_nullable_to_non_nullable
              as String,
      PARCIAL_1: PARCIAL_1 == freezed
          ? _value.PARCIAL_1
          : PARCIAL_1 // ignore: cast_nullable_to_non_nullable
              as String,
      BIENESTAR: BIENESTAR == freezed
          ? _value.BIENESTAR
          : BIENESTAR // ignore: cast_nullable_to_non_nullable
              as int,
      PARCIAL_2: PARCIAL_2 == freezed
          ? _value.PARCIAL_2
          : PARCIAL_2 // ignore: cast_nullable_to_non_nullable
              as String,
      PARCIAL_3: PARCIAL_3 == freezed
          ? _value.PARCIAL_3
          : PARCIAL_3 // ignore: cast_nullable_to_non_nullable
              as String,
      NOTA_HABILIT: NOTA_HABILIT == freezed
          ? _value.NOTA_HABILIT
          : NOTA_HABILIT // ignore: cast_nullable_to_non_nullable
              as String,
      NOTA_CRVER: NOTA_CRVER == freezed
          ? _value.NOTA_CRVER
          : NOTA_CRVER // ignore: cast_nullable_to_non_nullable
              as String,
      NOTA_DEF: NOTA_DEF == freezed
          ? _value.NOTA_DEF
          : NOTA_DEF // ignore: cast_nullable_to_non_nullable
              as String,
      GRUPO_ID: GRUPO_ID == freezed
          ? _value.GRUPO_ID
          : GRUPO_ID // ignore: cast_nullable_to_non_nullable
              as String,
      MATERIA_ID: MATERIA_ID == freezed
          ? _value.MATERIA_ID
          : MATERIA_ID // ignore: cast_nullable_to_non_nullable
              as String,
      DESC_MATERIA: DESC_MATERIA == freezed
          ? _value.DESC_MATERIA
          : DESC_MATERIA // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_ModelDataGradesCopyWith<$Res>
    implements $ModelDataGradesCopyWith<$Res> {
  factory _$$_ModelDataGradesCopyWith(
          _$_ModelDataGrades value, $Res Function(_$_ModelDataGrades) then) =
      __$$_ModelDataGradesCopyWithImpl<$Res>;
  @override
  $Res call(
      {String ESTUDIANTE_ID,
      String PARCIAL_1,
      int BIENESTAR,
      String PARCIAL_2,
      String PARCIAL_3,
      String NOTA_HABILIT,
      String NOTA_CRVER,
      String NOTA_DEF,
      String GRUPO_ID,
      String MATERIA_ID,
      String DESC_MATERIA});
}

/// @nodoc
class __$$_ModelDataGradesCopyWithImpl<$Res>
    extends _$ModelDataGradesCopyWithImpl<$Res>
    implements _$$_ModelDataGradesCopyWith<$Res> {
  __$$_ModelDataGradesCopyWithImpl(
      _$_ModelDataGrades _value, $Res Function(_$_ModelDataGrades) _then)
      : super(_value, (v) => _then(v as _$_ModelDataGrades));

  @override
  _$_ModelDataGrades get _value => super._value as _$_ModelDataGrades;

  @override
  $Res call({
    Object? ESTUDIANTE_ID = freezed,
    Object? PARCIAL_1 = freezed,
    Object? BIENESTAR = freezed,
    Object? PARCIAL_2 = freezed,
    Object? PARCIAL_3 = freezed,
    Object? NOTA_HABILIT = freezed,
    Object? NOTA_CRVER = freezed,
    Object? NOTA_DEF = freezed,
    Object? GRUPO_ID = freezed,
    Object? MATERIA_ID = freezed,
    Object? DESC_MATERIA = freezed,
  }) {
    return _then(_$_ModelDataGrades(
      ESTUDIANTE_ID: ESTUDIANTE_ID == freezed
          ? _value.ESTUDIANTE_ID
          : ESTUDIANTE_ID // ignore: cast_nullable_to_non_nullable
              as String,
      PARCIAL_1: PARCIAL_1 == freezed
          ? _value.PARCIAL_1
          : PARCIAL_1 // ignore: cast_nullable_to_non_nullable
              as String,
      BIENESTAR: BIENESTAR == freezed
          ? _value.BIENESTAR
          : BIENESTAR // ignore: cast_nullable_to_non_nullable
              as int,
      PARCIAL_2: PARCIAL_2 == freezed
          ? _value.PARCIAL_2
          : PARCIAL_2 // ignore: cast_nullable_to_non_nullable
              as String,
      PARCIAL_3: PARCIAL_3 == freezed
          ? _value.PARCIAL_3
          : PARCIAL_3 // ignore: cast_nullable_to_non_nullable
              as String,
      NOTA_HABILIT: NOTA_HABILIT == freezed
          ? _value.NOTA_HABILIT
          : NOTA_HABILIT // ignore: cast_nullable_to_non_nullable
              as String,
      NOTA_CRVER: NOTA_CRVER == freezed
          ? _value.NOTA_CRVER
          : NOTA_CRVER // ignore: cast_nullable_to_non_nullable
              as String,
      NOTA_DEF: NOTA_DEF == freezed
          ? _value.NOTA_DEF
          : NOTA_DEF // ignore: cast_nullable_to_non_nullable
              as String,
      GRUPO_ID: GRUPO_ID == freezed
          ? _value.GRUPO_ID
          : GRUPO_ID // ignore: cast_nullable_to_non_nullable
              as String,
      MATERIA_ID: MATERIA_ID == freezed
          ? _value.MATERIA_ID
          : MATERIA_ID // ignore: cast_nullable_to_non_nullable
              as String,
      DESC_MATERIA: DESC_MATERIA == freezed
          ? _value.DESC_MATERIA
          : DESC_MATERIA // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ModelDataGrades implements _ModelDataGrades {
  const _$_ModelDataGrades(
      {required this.ESTUDIANTE_ID,
      required this.PARCIAL_1,
      required this.BIENESTAR,
      required this.PARCIAL_2,
      required this.PARCIAL_3,
      required this.NOTA_HABILIT,
      required this.NOTA_CRVER,
      required this.NOTA_DEF,
      required this.GRUPO_ID,
      required this.MATERIA_ID,
      required this.DESC_MATERIA});

  factory _$_ModelDataGrades.fromJson(Map<String, dynamic> json) =>
      _$$_ModelDataGradesFromJson(json);

  @override
  final String ESTUDIANTE_ID;
  @override
  final String PARCIAL_1;
  @override
  final int BIENESTAR;
  @override
  final String PARCIAL_2;
  @override
  final String PARCIAL_3;
  @override
  final String NOTA_HABILIT;
  @override
  final String NOTA_CRVER;
  @override
  final String NOTA_DEF;
  @override
  final String GRUPO_ID;
  @override
  final String MATERIA_ID;
  @override
  final String DESC_MATERIA;

  @override
  String toString() {
    return 'ModelDataGrades(ESTUDIANTE_ID: $ESTUDIANTE_ID, PARCIAL_1: $PARCIAL_1, BIENESTAR: $BIENESTAR, PARCIAL_2: $PARCIAL_2, PARCIAL_3: $PARCIAL_3, NOTA_HABILIT: $NOTA_HABILIT, NOTA_CRVER: $NOTA_CRVER, NOTA_DEF: $NOTA_DEF, GRUPO_ID: $GRUPO_ID, MATERIA_ID: $MATERIA_ID, DESC_MATERIA: $DESC_MATERIA)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ModelDataGrades &&
            const DeepCollectionEquality()
                .equals(other.ESTUDIANTE_ID, ESTUDIANTE_ID) &&
            const DeepCollectionEquality().equals(other.PARCIAL_1, PARCIAL_1) &&
            const DeepCollectionEquality().equals(other.BIENESTAR, BIENESTAR) &&
            const DeepCollectionEquality().equals(other.PARCIAL_2, PARCIAL_2) &&
            const DeepCollectionEquality().equals(other.PARCIAL_3, PARCIAL_3) &&
            const DeepCollectionEquality()
                .equals(other.NOTA_HABILIT, NOTA_HABILIT) &&
            const DeepCollectionEquality()
                .equals(other.NOTA_CRVER, NOTA_CRVER) &&
            const DeepCollectionEquality().equals(other.NOTA_DEF, NOTA_DEF) &&
            const DeepCollectionEquality().equals(other.GRUPO_ID, GRUPO_ID) &&
            const DeepCollectionEquality()
                .equals(other.MATERIA_ID, MATERIA_ID) &&
            const DeepCollectionEquality()
                .equals(other.DESC_MATERIA, DESC_MATERIA));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(ESTUDIANTE_ID),
      const DeepCollectionEquality().hash(PARCIAL_1),
      const DeepCollectionEquality().hash(BIENESTAR),
      const DeepCollectionEquality().hash(PARCIAL_2),
      const DeepCollectionEquality().hash(PARCIAL_3),
      const DeepCollectionEquality().hash(NOTA_HABILIT),
      const DeepCollectionEquality().hash(NOTA_CRVER),
      const DeepCollectionEquality().hash(NOTA_DEF),
      const DeepCollectionEquality().hash(GRUPO_ID),
      const DeepCollectionEquality().hash(MATERIA_ID),
      const DeepCollectionEquality().hash(DESC_MATERIA));

  @JsonKey(ignore: true)
  @override
  _$$_ModelDataGradesCopyWith<_$_ModelDataGrades> get copyWith =>
      __$$_ModelDataGradesCopyWithImpl<_$_ModelDataGrades>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ModelDataGradesToJson(
      this,
    );
  }
}

abstract class _ModelDataGrades implements ModelDataGrades {
  const factory _ModelDataGrades(
      {required final String ESTUDIANTE_ID,
      required final String PARCIAL_1,
      required final int BIENESTAR,
      required final String PARCIAL_2,
      required final String PARCIAL_3,
      required final String NOTA_HABILIT,
      required final String NOTA_CRVER,
      required final String NOTA_DEF,
      required final String GRUPO_ID,
      required final String MATERIA_ID,
      required final String DESC_MATERIA}) = _$_ModelDataGrades;

  factory _ModelDataGrades.fromJson(Map<String, dynamic> json) =
      _$_ModelDataGrades.fromJson;

  @override
  String get ESTUDIANTE_ID;
  @override
  String get PARCIAL_1;
  @override
  int get BIENESTAR;
  @override
  String get PARCIAL_2;
  @override
  String get PARCIAL_3;
  @override
  String get NOTA_HABILIT;
  @override
  String get NOTA_CRVER;
  @override
  String get NOTA_DEF;
  @override
  String get GRUPO_ID;
  @override
  String get MATERIA_ID;
  @override
  String get DESC_MATERIA;
  @override
  @JsonKey(ignore: true)
  _$$_ModelDataGradesCopyWith<_$_ModelDataGrades> get copyWith =>
      throw _privateConstructorUsedError;
}
