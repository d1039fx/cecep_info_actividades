import 'package:flutter/material.dart';

class GradesView extends StatelessWidget {
  final String titleGrade, grade;

  const GradesView({Key? key, required this.titleGrade, required this.grade})
      : super(key: key);

  static TextStyle title = TextStyle(fontSize: 18, color: Colors.white);
  static TextStyle grades =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.white);

  Color infoGrades({required String value}) {
    double dataGrades = double.parse(value);
    if (dataGrades == 0.0) {
      return Colors.grey;
    } else if (dataGrades <= 2.9) {
      return Colors.red;
    } else if (dataGrades >= 3.0 && dataGrades <= 3.6) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
            decoration: BoxDecoration(
                color: infoGrades(value: grade),
                border: Border.all(color: Colors.transparent),
                borderRadius: BorderRadius.circular(8)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(grade.replaceAll(' ', ''), style: grades),
                Text(
                  titleGrade,
                  style: title,
                ),
              ],
            ),
          );
  }
}
