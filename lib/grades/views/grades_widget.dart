import 'package:claseinfoactividades/grades/models/model_data_grades.dart';
import 'package:claseinfoactividades/grades/views/modules/grades_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../presenters/grades_data/grades_data_bloc.dart';

class GradesWidget extends StatelessWidget {
  GradesWidget({Key? key}) : super(key: key);

  final TextStyle title = TextStyle(fontSize: 18, color: Colors.white);
  final TextStyle grades =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.white);

  Color infoGrades({required String value}) {
    double dataGrades = double.parse(value);
    if (dataGrades == 0.0) {
      return Colors.grey;
    } else if (dataGrades <= 2.9) {
      return Colors.red;
    } else if (dataGrades >= 3.0 && dataGrades <= 3.6) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  final SizedBox separator = SizedBox(
    width: 5,
  );

  final SizedBox separatorHeight = SizedBox(
    height: 5,
  );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GradesDataBloc, GradesDataState>(
      builder: (context, state) {
        ModelDataGrades modelDataGrades = state.modelDataGrades;
        return Container(
          height: 200,
          margin: const EdgeInsets.all(5),
          child: state.modelDataGrades.MATERIA_ID.isEmpty
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: [
                    modelDataGrades.PARCIAL_1 == ' 0.0'
                        ? SizedBox()
                        : Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          modelDataGrades.PARCIAL_1 == ' 0.0'
                              ? SizedBox()
                              : Expanded(
                            child: GradesView(
                                titleGrade: 'Parcial 1',
                                grade: modelDataGrades.PARCIAL_1),
                          ),
                          separator,
                          modelDataGrades.PARCIAL_2 == ' 0.0'
                              ? SizedBox()
                              : Expanded(
                            child: GradesView(
                                titleGrade: 'Parcial 2',
                                grade: modelDataGrades.PARCIAL_2),
                          ),
                          separator,
                          modelDataGrades.PARCIAL_3 == ' 0.0'
                              ? SizedBox()
                              : Expanded(
                            child: GradesView(
                                titleGrade: 'Parcial 3',
                                grade: modelDataGrades.PARCIAL_3),
                          ),
                        ],
                      ),
                    ),
                    separatorHeight,
                    modelDataGrades.NOTA_HABILIT == ' 0.0' && modelDataGrades.NOTA_CRVER == ' 0.0'
                        ? SizedBox()
                        : Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          modelDataGrades.NOTA_HABILIT == ' 0.0'
                              ? SizedBox()
                              : Expanded(
                              child: GradesView(
                                  titleGrade: 'Habilitacion',
                                  grade: modelDataGrades.NOTA_HABILIT)
                          ),
                          separator,
                          modelDataGrades.NOTA_CRVER == ' 0.0'
                              ? SizedBox()
                              : Expanded(
                            child: GradesView(
                                titleGrade: 'Curso de verano',
                                grade: modelDataGrades.NOTA_CRVER),
                          ),
                        ],
                      ),
                    ),
                    separatorHeight,
                    Expanded(
                      child: Container(
                        constraints: BoxConstraints.expand(),
                        child: GradesView(
                            titleGrade: 'Definitiva',
                            grade: modelDataGrades.NOTA_DEF),
                      ),
                    )
                  ],
                ),
        );
      },
    );
  }
}
