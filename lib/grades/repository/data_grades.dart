import 'package:claseinfoactividades/clases/constantes.dart';
import 'package:http/http.dart' as http;

class DataGrades extends Constantes {
  Future<http.Response> dataGradesGet(
      {required String codigo, required String plan}) async {
    Uri url = Uri.https(urlCecep, grades, {'codigo': codigo, 'cod_plan': plan});
    http.Response respuesta = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    return respuesta;
  }
}
