sealed class Resultado<T, E>{}

class Exito<T, E> extends Resultado<T, E>{
  final T value;

  Exito({required this.value});
}

class Error<T, E> extends Resultado<T, E>{
  final E value;

  Error({required this.value});
}