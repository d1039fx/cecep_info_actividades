import 'dart:convert';

import 'package:flutter/services.dart';

import 'mostrar_datos_login_model.dart';
import 'mostrar_info_salon_model.dart';

class MostrarDatos {
  Map<String, String> dias = {
    '1': 'Lunes',
    '2': 'Martes',
    '3': 'Miercoles',
    '4': 'Jueves',
    '5': 'Viernes',
    '6': 'Sabado',
    '7': 'Domingo'
  };

  List<InformacionUsuario> parseInfo(String respuesta) {
    final List<Map<String, dynamic>> parsed =
        jsonDecode(respuesta).cast<Map<String, dynamic>>();
    parsed.forEach((element) {
      if (element['DIAS'] == 'Lunes') {
        element['DIAS'] = '1';
      }
      if (element['DIAS'] == 'Martes') {
        element['DIAS'] = '2';
      }
      if (element['DIAS'] == 'Miercoles') {
        element['DIAS'] = '3';
      }
      if (element['DIAS'] == 'Jueves') {
        element['DIAS'] = '4';
      }
      if (element['DIAS'] == 'Viernes') {
        element['DIAS'] = '5';
      }
      if (element['DIAS'] == 'Sabado') {
        element['DIAS'] = '6';
      }
      if (element['DIAS'] == 'Domingo') {
        element['DIAS'] = '7';
      }
    });
    parsed.sort((a, b) => a['DIAS'].toString().compareTo(b['DIAS'].toString()));
    return parsed
        .map<InformacionUsuario>((valor) => InformacionUsuario.fromJson(valor))
        .toList();
  }

  InfoSalon parseInfoSalon({required String respuesta, required int idSalon}) {
    final List<Map<String, dynamic>> parsed =
        jsonDecode(respuesta).cast<Map<String, dynamic>>();
    Map<String, dynamic> jsonMap = {};
    parsed.forEach((element) {
      if (element['ESCENARIO_ID'] == idSalon) {
        jsonMap = element;
      }
    });
    InfoSalon infoSalon = InfoSalon.fromJson(jsonMap);
    return infoSalon;
  }

  List<InfoSalon> parseInfoEscenarios(
      {required List<Map<String, dynamic>> listaEscenarios}) {
    return listaEscenarios
        .map<InfoSalon>((e) => InfoSalon.fromJson(e))
        .toList();
  }

  Future<List<dynamic>> dataSalonPosition() async {
    String data =
    await rootBundle.loadString('assets/ubicaciones/escenarios.txt');
    return json.decode(data);
  }
}
