class InfoSalonQrModel {
  final String? idAlterna,
      horaInicio,
      horaFinal,
      descPlan,
      idMateria,
      descMateria,
      idGrupo,
      docente;

  final int? idEscenario, idPlanEstudio;

  InfoSalonQrModel(
      {required this.idEscenario,
      required this.idAlterna,
      required this.horaInicio,
      required this.horaFinal,
      required this.idPlanEstudio,
      required this.descPlan,
      required this.idMateria,
      required this.descMateria,
      required this.idGrupo,
      required this.docente});

  Map<String, dynamic> toMapModelInfoSalonQr() => {
        "ESCENARIO_ID": idEscenario,
        "ID_ALTERNA": idAlterna,
        "HORA_INICIO": horaInicio,
        "HORA_FIN": horaFinal,
        "PLAN_ESTUDIO_ID": idPlanEstudio,
        "DESC_PLAN": descPlan,
        "MATERIA_ID": idMateria,
        "DESC_MATERIA": descMateria,
        "GRUPO_ID": idGrupo,
        "DOCENTE": docente
      };

  factory InfoSalonQrModel.fromJson({required Map<String, dynamic> data}) =>
      InfoSalonQrModel(
        idEscenario: data['ESCENARIO_ID'],
        idAlterna: data['ID_ALTERNA'],
        horaInicio: data['HORA_INICIO'],
        horaFinal: data['HORA_FIN'],
        idPlanEstudio: data['PLAN_ESTUDIO_ID'],
        descPlan: data['DESC_PLAN'],
        idMateria: data['MATERIA_ID'],
        descMateria: data['DESC_MATERIA'],
        idGrupo: data['GRUPO_ID'],
        docente: data['DOCENTE'],
      );
}
