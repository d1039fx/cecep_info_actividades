import 'dart:ui';

import 'package:flutter/material.dart';



class DibujarMapa extends CustomPainter{

  final double lat, long, height, width;

  DibujarMapa(this.lat, this.long, this.height, this.width);

  Paint _paint = Paint();

  double pos({required double latitud}){
    double v1 = latitud - 3.428000;
    double v2 = (v1 * 1000000) - 558;
//    print(v1);
//    print(v2);
    return v2;
  }


  @override
  void paint(Canvas canvas, Size size) {




//// from x y to lat long
//    Coordinate latLng = Converter.convert.gridToLatLng(coordinate);
//    print(latLng.lat.toString()); // 22.281930012478536
//    print(latLng.lng.toString()); // 114.15824821181121

// from lat long to x y



    print(lat);
//    print(size.height);
//    print((size.height / (long * -1)) + 69);
//    print('lat1: ${(lat - 3.4) * 10000}, lat2: $lat');
//    print('long1: ${-(long + 76.5) * 10000}, long2: $long');
    List<Offset> _listaDePuntos = [
      //Offset(lat * 40, long * -1.7),
    Offset(0, 0),
    //Offset((lat - 3.4) * 10000, -(long + 76.5) * 10000),
    //Offset(((size.width / lat) + 10), (-(size.height / (long * -1)) - 69) * -1.7),
    Offset(pos(latitud: 3.428831), 105),
//    Offset(300, 75),
//    Offset(320, 200),
//    Offset(89, 125)
    ];
    _paint..color = Colors.blue
      ..strokeCap = StrokeCap.round
        ..strokeWidth = 10;

    canvas.drawPoints(PointMode.points, _listaDePuntos, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate)=> true;

}