import 'dart:convert';

import 'package:claseinfoactividades/clases/info_salon_qr_model.dart';
import 'package:http/http.dart' as http;

class ObtenerDatosSalonQr {
  Future<List<InfoSalonQrModel>> obtenerDatosQr({required String codigo}) async {
    Uri url =
        Uri.https('servicios.cecep.edu.co', '/consultas/salones_json.asp',
            {'salon': codigo});

    http.Response respuesta = await http.post(url);
    List<Map<String, dynamic>> listaDatosHorarioSalonJson =
        jsonDecode(respuesta.body).cast<Map<String, dynamic>>();
    List<InfoSalonQrModel> listaDatosHorariosSalonModel =
        listaDatosHorarioSalonJson
            .map((e) => InfoSalonQrModel.fromJson(data: e))
            .toList();
    return listaDatosHorariosSalonModel;
  }
}
