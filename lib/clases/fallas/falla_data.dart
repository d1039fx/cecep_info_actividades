class FallaData implements Exception{
  final String message;

  FallaData({required this.message});
}