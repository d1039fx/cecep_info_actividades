import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class MostrarImagenesLugares {
  Future<String> obtenerImagenesUrl({required int idLugar}) async {
    Uri uri = Uri.https(
        'moodle.cecep.edu.co', '/autoevaluacion/imagenes/$idLugar/f2.jpg');

    String urlImagen = kIsWeb ? await http.get(uri, headers: {
      "Accept": "application/json",
      "Access-Control_Allow_Origin": "*"
    }).then((value) {
      if (value.statusCode == 200) {
        return '${uri.origin}${uri.path}';
      }
      return value.statusCode.toString();
    }) : await http.get(uri).then((value) {
      if (value.statusCode == 200) {
        return '${uri.origin}${uri.path}';
      }
      return value.statusCode.toString();
    });
    return urlImagen;
  }
}
