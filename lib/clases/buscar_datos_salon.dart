import 'package:claseinfoactividades/clases/mostrar_info_salon_model.dart';

import 'mostrar_datos.dart';

class BuscarDatosSalon {
  MostrarDatos _mostrarDatos = MostrarDatos();

  List<InfoSalon> buscarSalon({required List<Map<String, dynamic>> listaEscenarios, required String escenario}) {
    List<InfoSalon> lista = _mostrarDatos
        .parseInfoEscenarios(listaEscenarios: listaEscenarios)
        .where((element){
          if(escenario[0] == 'A'){
            return element.descEscenario.contains(escenario);
          }else if(escenario[0] == 'Q'){
            return element.descEscenario.contains(escenario);
          }else{
            return element.descEscenario.contains(escenario.replaceFirst(RegExp('u'), 'ú'));
          }
    })
        .toList();
    return lista;
  }
}
