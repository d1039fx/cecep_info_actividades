import 'dart:async';

import 'package:claseinfoactividades/clases/info_salon_qr_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class RelojProcesos extends GetxController {
  String _timeOfDayResult = '';

  String get timeOfDayResultGet => _timeOfDayResult;

  Timer timeOfDayResultSet(BuildContext context) {
   return Timer.periodic(Duration(seconds: 1), (timer) {
      _timeOfDayResult = TimeOfDay.now().format(context);
      update();
    });
  }

  String stateClass(int pos){
    List<String> state = [
      'Clases Anteriores',
      'En Clase',
      'Clases Siguientes'
    ];
    return state[pos];
  }

  Color stateColorClass(int index){
    List<Color> stateColor = [
      Colors.red[100]!,
      Colors.green[100]!,
      Colors.yellow[100]!
    ];
    return stateColor[index];
  }

  List<List<InfoSalonQrModel>> listSalonState(
      {required List<InfoSalonQrModel> infoSalonQrModel,
      required BuildContext context}) {
    List<InfoSalonQrModel> beforeClass = [];
    List<InfoSalonQrModel> currentClass = [];
    List<InfoSalonQrModel> afterClass = [];
    List<List<InfoSalonQrModel>> listStateSalon = [];
    infoSalonQrModel.forEach((element) {
      bool iniClass =
          diferenceTime(context: context, timeClass: element.horaInicio);
      bool finClass =
          diferenceTime(context: context, timeClass: element.horaFinal);
      if (iniClass && finClass) {
        beforeClass.add(element);
      } else if (iniClass && !finClass) {
        currentClass.add(element);
      } else {
        afterClass.add(element);
      }
    });
    if (beforeClass.isEmpty) {
      listStateSalon = [[], currentClass, afterClass];
    } else if (afterClass.isEmpty) {
      listStateSalon = [beforeClass, currentClass, []];
    } else {
      listStateSalon = [beforeClass, currentClass, afterClass];
    }
    return listStateSalon;
  }


  bool diferenceTime({required BuildContext context, required String timeClass}) {
    DateTime before = DateFormat.Hm('en_US').parse(timeClass);
    DateTime current = DateFormat.Hm('en_US').parse(timeOfDayResultGet);
    return before.difference(current).isNegative;
  }
}
