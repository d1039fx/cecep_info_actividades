class InformacionUsuario {
  final String documento,
      nombre,
      id,
      plan,
      codigo,
      nombreMateria,
      idSalon,
      salon,
      horaInicial,
      horaFinal,
      dia;
  final int semestre;

  InformacionUsuario(
      {required this.documento,
      required this.nombre,
      required this.dia,
      required this.id,
      required this.semestre,
      required this.plan,
      required this.codigo,
      required this.nombreMateria,
      required this.idSalon,
      required this.salon,
      required this.horaInicial,
      required this.horaFinal});

  factory InformacionUsuario.fromJson(Map<String, dynamic> json) {
    return InformacionUsuario(
        documento: json['DOC_IDENT'].toString(),
        id: json['ESTUDIANTE_ID'],
        nombre: json['NOM_ESTUD'],
        semestre: json['SEMESTRE'],
        plan: json['DESC_PLAN'],
        codigo: json['ESTUDIANTE_ID'],
        nombreMateria: json['DESC_MATERIA'],
        idSalon: json['ESCENARIO_ID'].toString(),
        salon: json['ESCENARIO'],
        horaInicial: json['HORA_INICIAL'],
        dia: json['DIAS'],
        horaFinal: json['HORA_FINAL']);
  }
}
