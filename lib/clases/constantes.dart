class Constantes{

  String urlCecep = 'servicios.cecep.edu.co';
  String grades = '/consultas/notas_json.asp';

  List<String> oficinas = [
    'Biblioteca',
    'Director General Academico',
    'Coordinación Academica',
    'Coordinación de Carreras',
    'Secretaria General',
    'Registro y Control',
    'Bienestar',
    'Enfermería',
    'Psicología',
    'Investigación'
  ];

  List<Map<String, dynamic>> extencionesOficinas = [
    {'nombre':'Biblioteca','ext': ['230']},
    {'nombre':'Director General Academico','ext': ['323']},
    {'nombre':'Coordinación Academica', 'ext':['204','206','203']},
    {'nombre':'Coordinación de Carreras', 'ext':[]},
    {'nombre':'Secretaria General', 'ext':['219']},
    {'nombre':'Registro y Control', 'ext':['214']},
    {'nombre':'Bienestar', 'ext':['289']},
    {'nombre':'Enfermería', 'ext':['250']},
    {'nombre':'Psicología', 'ext':['340']},
    {'nombre':'Investigación', 'ext':['320']}
  ];

}