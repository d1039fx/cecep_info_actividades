class InfoSalon {
  final String? descEscenario, descTipoEscenario, descEdificio, idAlternoEscenario, estadoEscenario;
  final int? idEdificio, capacidadEscenario, idEscenario, idTipoEscenario;

  InfoSalon(
      {required this.idEscenario,
      required this.descEscenario,
      required this.descTipoEscenario,
      required this.idEdificio,
      required this.descEdificio,
      required this.capacidadEscenario,
      required this.idAlternoEscenario,
      required this.idTipoEscenario,
      required this.estadoEscenario});

  factory InfoSalon.fromJson(Map<String, dynamic> json) {
    return InfoSalon(
        idEscenario: json['ESCENARIO_ID'],
        descEscenario: json['DESC_ESCENARIO'],
        descTipoEscenario: json['DESC_TIPO_ESCENARIO'],
        idEdificio: json['EDIFICIO_ID'],
        descEdificio: json['DESC_EDIFICIO'],
        capacidadEscenario: json['CAPACIDAD'],
        idAlternoEscenario: json['ID_ALTERNA'],
        idTipoEscenario: json['TIPO_ESCENARIO_ID'],
        estadoEscenario: json['ESTADO']);
  }
}
