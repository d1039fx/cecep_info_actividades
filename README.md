# Info Actividades

Aplicación para informar al estudiante de sus clases, los horarios y   
la ubicacion de los salones que corresponden a las mismas.

## Login

El estudiante ingresa su numero de cedula y su codigo.

![login](/images_readme/login.png "Login")

## Pagina Principal
En esta seccion se muestra la lista de salones que corresponde al estudiante con sus respectivos horarios y ubicacion.

![salones](/images_readme/lista_de_salones.png "Salones")

## Mapa
En el momento en que se presiona el salon se muestra una imagen donde se ubica   
con un punto rojo el salon dentro de una mapa que corresponde al **Cecep**.

![boton_salones](/images_readme/boton_ubicacion_salon.png "Salones") ![between](/images_readme/between_pages.png)  ![mapa](/images_readme/mapa_cecep.png "boton")

## Datos Estudiante

En el menu de opciones se muestran los datos del estudiante

![mapa](/images_readme/boton_datos_usuario.png "mapa") ![between](/images_readme/between_pages.png) ![mapa](/images_readme/datos_estudiante.png "datos estudiante")

## Ver Salon

En esta seccion se muestra los datos del salon leidos por un codigo qr

![datos_salon](/images_readme/boton_datos_salon.png) ![between](/images_readme/between_pages.png) ![datos_salon](/images_readme/datos_salon.png)

## Buscar Salon

En esta seccion se puede buscar salon dando como   
resultado una lista de los salones que correspondan   
con la busqueda

![datos_salon](/images_readme/boton_buscar_salon.png) ![between](/images_readme/between_pages.png) ![datos_salon](/images_readme/buscar_salon.png)
